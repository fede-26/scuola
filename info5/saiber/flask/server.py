from flask import Flask, request
import time

app = Flask(__name__)

OTPs = dict()

from random import randint

def generate():
    rand = randint(100000, 999999)
    token = str(rand)
    token = token[:3] + '-' + token[3:]
    print(token)
    return token

@app.route("/")
def login():
    return """
<h1>QUESTA È LA LOGIN</h1>
<form method='post' action='/post-login'>
    Email: <input name='email' type='text'/><br>
    Password: <input name='password' type='password'/><br>
    <input type='submit' value='Enter'/>
</form>
    """

@app.route("/post-login", methods=['POST'])
def post_login():
    email = request.form['email']
    password = request.form['password']

    # query db for password
    password_giusta = "123456"

    if password == password_giusta:
        #send email with code
        #save code
        OTPs[email] = (generate(), time.time())
        print(f'otp for {email} = {OTPs[email]}')

        return f"""
<h1>Controlla la mail per il token</h1>
<form method='post' action='/otp-control'>
    TOKEN: <input name='otp' type='text'/><br>
    <input name='email' value='{request.form['email']}' type='hidden'/>
    <input type='submit' value='Enter'/>
</form>
"""

    return "password sbagliata"
    
@app.route("/otp-control", methods=['POST'])
def otp_control():
    token, timestamp = OTPs[request.form['email']]
    OTPs[request.form['email']] = None
    if request.form['otp'] == token:
        if timestamp < time.time() - 60:
            return "OTP scaduto"
        return "LOGGED IN"
    return 'OTP sbagliato'
