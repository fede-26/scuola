<%-- 
    Document   : form
    Created on : 5-ott-2022, 12.12.00
    Author     : federico.zotti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="login.jsp" method="post">
            username: <input type="text" name="user" ><br>
            password: <input type="password" name="pwd"> <br><br>
            cognome <input type="text" name="cognome" ><br>
            <input type="submit" value="Login">
        </form>
        <%
            String messaggio=request.getParameter("messaggio");
            if(messaggio==null)
                messaggio="";
            if (!messaggio.equals("")){
                %>
                <p style="color:red"><%=messaggio%></p>
                <%
            }
            %>
    </body>
</html>
