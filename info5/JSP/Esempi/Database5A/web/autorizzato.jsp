<%-- 
    Document   : autorizzato
    Created on : 11-ott-2022, 11.52.06
    Author     : giuseppe.depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Pagina riservata</h1>
        <%
            String user=(String)session.getAttribute("user");
            if(user==null)
                user="";
            if (!user.equals("")){
                out.write("Benvenuto"+user);
            }else{
                response.sendRedirect("form.jsp?messaggio=Effettuare il login");
            }
            

            %>
    </body>
</html>
