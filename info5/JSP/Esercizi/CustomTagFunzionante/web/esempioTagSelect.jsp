<%-- 
    Document   : esempioTagSelect
    Created on : 9-feb-2018, 9.44.26
    Author     : depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="WEB-INF/tlds/customTag.tld" prefix="dp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <dp:select nome="auto">
		A146=Alfa 146,
		RAV4=Toyota Rav 4,
		RLAG=Renault Laguna,
		FPAN=Fiat Panda,
	</dp:select>
    </body>
</html>
