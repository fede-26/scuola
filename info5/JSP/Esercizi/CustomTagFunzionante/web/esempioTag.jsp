<%-- 
    Document   : esempioTag
    Created on : 9-feb-2018, 8.55.01
    Author     : depietro
--%>
<%@ taglib uri="WEB-INF/tlds/customTag.tld" prefix="dp"%> 
 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@taglib prefix="dpEsempio" uri="/WEB-INF/tlds/customTag.tld" %>
        <%@taglib prefix="dpDatabase" uri="/WEB-INF/tlds/dpDatabase.tld" %>
        
        <dpDatabase:connetti driver="MYSQL" database="localhost/itinerari" password="" user="root">
            <dpDatabase:combo nome="ciclisti" tabella="salite" text="nome" value="codSalita" />
            
            
        </dpDatabase:connetti>
        
        <dpEsempio:PrimoTag />
        
        <h1>Hello World!</h1>
       
    </body>
</html>
