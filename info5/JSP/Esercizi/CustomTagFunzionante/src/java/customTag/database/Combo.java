/*
 * Created on 24-ago-2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package customTag.database;

import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.TagSupport;
import java.sql.*;
/**
 * @author depietro
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Combo extends TagSupport{
	private String tabella="";
	private String nome="";
	private String value="";
	private String text="";
	/**
	 * @param nome Imposta il valore della propriet� nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @param tabella Imposta il valore della propriet� tabella.
	 */
	public void setTabella(String tabella) {
		this.tabella = tabella;
	}
	/**
	 * @param text Imposta il valore della propriet� text.
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @param value Imposta il valore della propriet� value.
	 */
	public void setValue(String value) {
		this.value = value;
	}

    @Override
    public int doEndTag() throws JspException {
        return super.doEndTag(); //To change body of generated methods, choose Tools | Templates.
    }
	
	public int doStartTag() throws JspException {
		// TODO doStartTag()
		try {
			String sql="SELECT "+ value+","+text+" FROM "+ tabella +" ";
			Connetti parent=(Connetti)getParent();
			Connection cn=parent.getCn();
			Statement stmt=cn.createStatement();
			ResultSet rs=stmt.executeQuery(sql);
			JspWriter out=pageContext.getOut();
			out.write("<SELECT name=\""+nome+"\" >");
			while (rs.next()){
				out.write("<OPTION value=\""+rs.getString(value)+"\" >");
				out.write(rs.getString(text)+"</OPTION>");
			}
			out.write("</SELECT>");
			stmt.close();
		} catch (Exception e) {
			throw new JspException ("Si � verificato il seguente errore: "+e.getMessage());
		}
		return SKIP_BODY;
	}
}
