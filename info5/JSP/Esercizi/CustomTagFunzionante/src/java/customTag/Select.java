/*
 * Created on 21-ago-2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package customTag;

import jakarta.servlet.jsp.*;
import jakarta.servlet.jsp.tagext.BodyTagSupport;
/**
 * @author depietro
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Select extends BodyTagSupport{
	
	private String nome="";
	
	/**
	 * @param nome Imposta il valore della proprieta' nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
//		recupero dell'oggetto out
		JspWriter out=pageContext.getOut();
		
		// recupero della stringa all'interno del body
		String dati=bodyContent.getString().trim();
		
		//elimino l'eventuale virgola finale
		if (dati.endsWith(",")) dati=dati.substring(0,dati.length()-1);
		// recupero degli elementi dalla stringa
		String[] elemento=dati.split(",");
		try{
			String valore,view;
			int indice;
			out.write("<SELECT name='"+nome+"'>");
			// scrittura del codice HTML con gli elementi inseriti
			for (int i=0;i<elemento.length;i++){
				valore="";
				view="";
				indice=elemento[i].indexOf("=");
				if (indice!=-1){
					valore=elemento[i].substring(0,indice);
					view=elemento[i].substring(indice+1);
				}
				else{
					valore=elemento[i];
					view=valore;
				}
				out.write("<OPTION value='"+valore+"'>"+view+"</OPTION>");
			}
			out.write("</SELECT>");
		}
		catch (Exception e){
			//gestisce qualsiasi tipo di errore e interrompe l'esecuzione della 
			//pagina JSP generando una JspException
			throw new JspException (e.getMessage());
		}
		return EVAL_PAGE; 
	
	
	}
}
