<%-- 
    Document   : elenco
    Created on : 26-ott-2022, 11.49.46
    Author     : giuseppe.depietro
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
             Class.forName("com.mysql.jdbc.Driver");
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost/profili", "root","password");
            Statement stmt=cn.createStatement();
            
            String sql="SELECT * FROM utenti";
            ResultSet rs=stmt.executeQuery(sql);
            
            while(rs.next()){
                out.write(rs.getString("cognome")+" ");
                out.write(rs.getString("nome")+"<br> ");
            }
            
            %>
    </body>
</html>
