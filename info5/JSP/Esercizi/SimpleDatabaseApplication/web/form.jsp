<%-- 
    Document   : form
    Created on : 11-ott-2022, 11.17.23
    Author     : giuseppe.depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Form login</h1>
        
        <form action="login.jsp" method="post">
            Username: <input type="text" name="user"><br>
            Password: <input type="password" name="pwd"><br><br>
            <input type="submit" value="Login">
            
        </form>
        <%
            String messaggio=request.getParameter("messaggio");
            if(messaggio==null)
                messaggio="";
            if(!messaggio.equals("")){
                out.write("<p style=color:red>"+messaggio+"</p>");
            }
            %>
        
    </body>
</html>
