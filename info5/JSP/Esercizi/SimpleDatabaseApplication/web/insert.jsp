<%-- 
    Document   : insert
    Created on : 26-ott-2022, 11.56.10
    Author     : giuseppe.depietro
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost/profili", "root","");
            Statement stmt=cn.createStatement();
            
            String sql="INSERT INTO utenti (username, password, cognome, nome, email) VALUES ('silvia1', 'segreta', 'Neri', 'Silvia', 'silvia@gmail.com');";
            stmt.executeUpdate(sql);
            }catch(SQLException e){
                out.write("Errore nell'inserimento dati");
            }
            
            
            %>
            
    </body>
</html>
