package models;

/**
 *
 * @author federico
 */
public class Squadra {
    String nome;
    int goalHome;
    int goalGuest;
    int vincite;

    public int getVincite() {
        return vincite;
    }

    public void addVincita() {
        this.vincite ++;
    }

    public String getNome() {
        return nome;
    }

    public int getGoalHome() {
        return goalHome;
    }

    public int getGoalGuest() {
        return goalGuest;
    }
    
    public int getGoalAll() {
        return goalHome + goalGuest;
    }

    public Squadra(String nome) {
        this.nome = nome;
    }
    
     public void addGoalHome(int x) {
        this.goalHome += x;
    }
     public void addGoalGuest(int x) {
        this.goalGuest += x;
    }
}
