<%-- 
    Document   : add-squadra
    Created on : Nov 16, 2022, 10:06:25 AM
    Author     : federico
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.Squadra"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aggiungi punteggi</title>
    </head>
    <body>
        <h1>Aggiungi punteggi ad una partita</h1>
    <%
        String id = request.getParameter("idPartita");
        String goal1 = request.getParameter("goal1");
        String goal2 = request.getParameter("goal2");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");

        String user = (String) session.getAttribute("user");
        if (user == null) {
            response.sendRedirect("login.jsp?messaggio=Inserire le credenziali");
        }
      
        out.write("Current logged in user: " + user);
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root","password");
            session.setAttribute("connection", cn);
        }

        //set condition true if all the params are filled
        Boolean conditionNull = id != null && goal1 != null && goal2 != null;
        Boolean condition = conditionNull && !(id.equals("") || goal1.equals("") || goal2.equals(""));
        Statement stmt=cn.createStatement();

        //add punteggio logic
        if (condition){

            String sqlAddPartita = "UPDATE partite SET goal1='"+goal1+"', goal2='"+goal2+"' WHERE codPartita='"+id+"';";
            int result = stmt.executeUpdate(sqlAddPartita);
            
            //aggiorna classifica
            ArrayList<Squadra> classifica = new ArrayList<Squadra>();
            String sqlClassifica = "SELECT * FROM partite";
            ResultSet rsClassifica = stmt.executeQuery(sqlClassifica);
            Squadra squadra;
            String vincitore;
            while (rsClassifica.next()){
                if (rsClassifica.getInt("goal1") > rsClassifica.getInt("goal2")){
                    vincitore = rsClassifica.getString("squadra1");
                } else {
                    vincitore = rsClassifica.getString("squadra2");
                }
                squadra = null;
                //controlla se la squadra e' gia' presente
                for (Squadra x: classifica){
                    if (vincitore.equals(x.getNome())){
                        squadra = x;
                        break;
                    }
                }

                if (squadra == null){
                    //aggiunge la squadra se non e' gia' presente
                    squadra = new Squadra(vincitore);
                    classifica.add(squadra);
                }
                squadra.addVincita();
            }
            
            String sqlUpdateClassifica;
            int result2;
            for (Squadra x: classifica){
                int vincite = x.getVincite();
       
                sqlUpdateClassifica = "UPDATE classifica SET punti='"+Integer.toString(vincite)+"' WHERE squadra='"+x.getNome()+"'";
                result2 = stmt.executeUpdate(sqlUpdateClassifica);
            }
            response.sendRedirect("add-punteggio.jsp?message=Partita aggiunta&messageColor=green");
           
        }
        
        if (message == null){
            message = "";
        }
        
            %>
            
            
        <%
        // Visualizza tutte le partite per scegliere
        
String sqlPartite = "SELECT * FROM partite";
    ResultSet rsPartite = stmt.executeQuery(sqlPartite);

    out.write("<table border=1px solid grey style='border-collapse: collapse'>");
    out.write("<tr><th>Data</th><th>Squadra 1</th><th>Squadra 2</th></tr>");
    while (rsPartite.next()) {
        out.write("<tr><td>" + rsPartite.getString("dataIncontro") + "</td><td>" + rsPartite.getString("squadra1") + " ["+rsPartite.getString("goal1")+"]" + "</td><td>" + rsPartite.getString("squadra2") + " ["+rsPartite.getString("goal2")+"]" + "</td>" );     
        out.write("<td><a href='add-punteggio.jsp?idPartita="+rsPartite.getString("codPartita")+"'>Seleziona</a></td></tr>" );     

            }
    out.write("</table>");
        %>

           <%
        // visualizzare il form se la squadra e' selezionata
        
String sqlPartitaSel = "SELECT * FROM partite WHERE codPartita='"+id+"'";
    ResultSet rsPartitaSel = stmt.executeQuery(sqlPartitaSel);

    if (rsPartitaSel.next()) {
        out.write("<h2>Partita del " + rsPartitaSel.getString("dataIncontro") + ": " + rsPartitaSel.getString("squadra1") + " - " + rsPartitaSel.getString("squadra2"));     
        out.write("</h2><form action='add-punteggio.jsp?idPartita="+rsPartitaSel.getString("codPartita")+"' method='post'>" );   
        out.write("Goal " + rsPartitaSel.getString("squadra1") +": <input type='text' name='goal1'><br>" );     
        out.write("Goal " + rsPartitaSel.getString("squadra2") +": <input type='text' name='goal2'><br>" ); 
        out.write("<input type='hidden' name='idPartita' value='"+id+"'><br>" );     

        out.write("<button type='submit'>Modifica</button>" );     
        out.write("</form>");     

            }
        %>
        
        
        
        
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>
       
        <br>
        <a href="private.jsp">Ritorna alla home admin</a>
    </body>
</html>
