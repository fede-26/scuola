<%-- 
    Document   : private
    Created on : Nov 9, 2022, 10:28:16 AM
    Author     : federico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin area</title>
    </head>
    <body>
        <h1>Cambio password</h1>
        <%
        String oldPasswd = request.getParameter("oldPasswd");
        String newPasswd = request.getParameter("newPasswd");
        String message = request.getParameter("message");
        String messageColor = request.getParameter("messageColor");


        String user = (String) session.getAttribute("user");
        if (user == null) {
            response.sendRedirect("login.jsp?messaggio=Inserire le credenziali");
        }
      
        out.write("Current logged in user: " + user);
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root","password");
            session.setAttribute("connection", cn);
        }

        //change password logic
        if (oldPasswd != null && newPasswd != null){
            String sqlGetPasswd = "SELECT * FROM utenti WHERE user='"+user+"' AND password='"+oldPasswd+"'";
            
            Statement stmt=cn.createStatement();
            ResultSet rs=stmt.executeQuery(sqlGetPasswd);
            if (rs.next()){
                String updatePasswd = "UPDATE utenti SET password='"+newPasswd+"' WHERE user='"+user+"'";
                int result = stmt.executeUpdate(updatePasswd);
                response.sendRedirect("cambio-pwd.jsp?message=Password cambiata&messageColor=green");

            } else {
                response.sendRedirect("cambio-pwd.jsp?message=Credenziali errate&messageColor=red");
            }
         }
         
        if (message == null){
            message = "";
        }
        
            %>
        <form action="cambio-pwd.jsp?" method="post">
            Old password: <input type="password" name="oldPasswd"><br>
            New password: <input type="password" name="newPasswd"><br>
            <input type="submit" value="Change">  
        </form>
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>
       
        <br>
        <a href="private.jsp">Ritorna alla home admin</a>
    </body>
</html>