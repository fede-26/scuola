<%-- 
    Document   : private
    Created on : Nov 9, 2022, 10:28:16 AM
    Author     : federico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin area</title>
    </head>
    <body>
        <h1>Admin area</h1>
        <%

        String user = (String) session.getAttribute("user");
//String user = "admin";
        if (user == null) {
            response.sendRedirect("login.jsp?messaggio=Inserire le credenziali");
        }
      
        out.write("Current logged in user: " + user);
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root","password");
            session.setAttribute("connection", cn);
            }
            %>
            <br>
        <ul>
            <li><a href="lista-squadre.jsp">Lista squadre</a></li>
            <li><a href="lista-partite.jsp">Lista partite</a></li>      
            <li><a href="classifica.jsp">Classifica</a></li>
            <br>
            <li><a href="add-squadra.jsp">Inserisci squadra</a></li>
            <li><a href="add-partita.jsp">Inserisci partita al calendario</a></li>
            <li><a href="add-punteggio.jsp">Modifica punteggi</a></li>

            <br>
            <li><a href="cambio-pwd.jsp">Cambia password</a></li>
            <br>
            <li><a href="logout.jsp">Logout</a></li>
        </ul>
    </body>
</html>