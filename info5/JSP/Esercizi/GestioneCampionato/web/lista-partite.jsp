<%-- 
    Document   : lista-squadre
    Created on : Nov 16, 2022, 9:08:39 AM
    Author     : federico
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Elenco partite</title>
    </head>
    <body>
        <h1>Elenco partite</h1>
        <%
    Connection cn = (Connection) session.getAttribute("connection");
    if (cn == null) {
        Class.forName("com.mysql.jdbc.Driver");
        cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root", "password");
        session.setAttribute("connection", cn);
    }

    Statement stmt = cn.createStatement();

    String sql = "SELECT * FROM partite";
    ResultSet rs = stmt.executeQuery(sql);

    out.write("<table border=1px solid grey style='border-collapse: collapse'>");
    out.write("<tr><th>Data</th><th>Squadra 1</th><th>Squadra 2</th></tr>");
    while (rs.next()) {
        out.write("<tr><td>" + rs.getString("dataIncontro") + "</td><td>" + rs.getString("squadra1") + " ["+rs.getString("goal1")+"]" + "</td><td>" + rs.getString("squadra2") + " ["+rs.getString("goal2")+"]" + "</td></tr>" );     
    }
    out.write("</table>");
        %>

                <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
