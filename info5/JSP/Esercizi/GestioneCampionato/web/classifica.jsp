<%-- 
    Document   : lista-squadre
    Created on : Nov 16, 2022, 9:08:39 AM
    Author     : federico
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.util.ArrayList"%>

<%@page import="models.Squadra"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Classifica</title>
    </head>
    <body>
        <h1>Classifica</h1>
        <%
            //Classifica normale
    Connection cn = (Connection) session.getAttribute("connection");
    if (cn == null) {
        Class.forName("com.mysql.jdbc.Driver");
        cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root", "password");
        session.setAttribute("connection", cn);
    }

    Statement stmt = cn.createStatement();

    String sql = "SELECT * FROM classifica ORDER BY punti DESC";
    ResultSet rs = stmt.executeQuery(sql);

    out.write("<table border=1px solid grey style='border-collapse: collapse'>");
    out.write("<tr><th>Squadra</th><th>Punti</th></tr>");
    while (rs.next()) {
        out.write("<tr><td>" + rs.getString("squadra") + "</td><td>" + rs.getString("punti") + "</td></tr>" );     
    }
    out.write("</table>");
        %>
        <br>
        <%
     //Squadra con piu' punti
    
String sql2 = "SELECT * FROM partite";
ResultSet rs2 = stmt.executeQuery(sql2);

ArrayList<Squadra> partite = new ArrayList<Squadra>();
Squadra squadra1;   //Home
Squadra squadra2;   //Guest

while (rs2.next()){
//SQUADRA 1 (HOME)
    squadra1 = null;
    //controlla se la squadra e' gia' presente
    for (Squadra x: partite){
        if (rs2.getString("squadra1").equals(x.getNome())){
            squadra1 = x;
            break;
        }
    }
    
    if (squadra1 == null){
        //aggiunge la squadra se non e' gia' presente
        squadra1 = new Squadra(rs2.getString("squadra1"));
        partite.add(squadra1);
    }
    
    //aggiunge i goal alla squadra Home (squadra 1)
    squadra1.addGoalHome(rs2.getInt("goal1"));
    
    //SQUADRA 2 (GUEST)
    squadra2 = null;
    //controlla se la squadra e' gia' presente
    for (Squadra x: partite){
        if (rs2.getString("squadra2").equals(x.getNome())){
            squadra2 = x;
            break;
        }
    }
    
    if (squadra2 == null){
        //aggiunge la squadra se non e' gia' presente
        squadra2 = new Squadra(rs2.getString("squadra2"));
        partite.add(squadra2);
    }
    
      //aggiunge i goal alla squadra Guest (squadra 2)
    squadra2.addGoalGuest(rs2.getInt("goal2"));
    
            
            }
    
out.write("<table border=1px solid grey style='border-collapse: collapse'>");
out.write("<tr><th>Squadra</th><th>Goal as Home</th><th>Goal as Guest</th><th>Goal Total</th></tr>");
for (Squadra x: partite) {
 out.write("<tr><td>" + x.getNome() + "</td><td>" + x.getGoalHome() + "</td><td>" + x.getGoalGuest() +  "</td><td>" + x.getGoalAll() + "</td></tr>" );     
}
out.write("</table>");
        %>
        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
