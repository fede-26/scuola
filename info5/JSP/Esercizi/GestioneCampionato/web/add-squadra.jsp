<%-- 
    Document   : add-squadra
    Created on : Nov 16, 2022, 10:06:25 AM
    Author     : federico
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aggiungi squadra</title>
    </head>
    <body>
        <h1>Aggiungi squadra</h1>
    <%
        String nomeSquadra = request.getParameter("nomeSquadra");
        String presidente = request.getParameter("presidente");
        String allenatore = request.getParameter("allenatore");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");

        String user = (String) session.getAttribute("user");
        if (user == null) {
            response.sendRedirect("login.jsp?messaggio=Inserire le credenziali");
        }
      
        out.write("Current logged in user: " + user);
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root","password");
            session.setAttribute("connection", cn);
        }

        //set condition true if all the params are filled
        Boolean conditionNull = nomeSquadra != null && allenatore != null && presidente != null;
        Boolean condition = conditionNull && !(nomeSquadra.equals("") || allenatore.equals("") || presidente.equals(""));

        //add squadra logic
        if (condition){
            String sqlGetSquadra = "SELECT * FROM squadre WHERE nome='"+nomeSquadra+"'";
            
            Statement stmt=cn.createStatement();
            ResultSet rs=stmt.executeQuery(sqlGetSquadra);
            if (rs.next()){
          
                response.sendRedirect("add-squadra.jsp?message=Squadra gia' presente&messageColor=red");

            } else {
                String sqlAddSquadra = "INSERT INTO squadre VALUE ('"+nomeSquadra+"','"+allenatore+"','"+presidente+"');";
                int result = stmt.executeUpdate(sqlAddSquadra);
                response.sendRedirect("add-squadra.jsp?message=Squadra aggiunta&messageColor=green");
            }
         }
         
        if (message == null){
            message = "";
        }
        
            %>
        <form action="add-squadra.jsp" method="post">
            Nome: <input type="text" name="nomeSquadra"><br>
            Presidente: <input type="text" name="presidente"><br>
            Allenatore: <input type="text" name="allenatore"><br>
            <input type="submit" value="Aggiungi">  
        </form>
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>
       

        <br>
        <a href="private.jsp">Ritorna alla home admin</a>
    </body>
</html>
