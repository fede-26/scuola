<%-- 
    Document   : add-squadra
    Created on : Nov 16, 2022, 10:06:25 AM
    Author     : federico
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="validators.DateValidatorUsingDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aggiungi partita</title>
    </head>
    <body>
        <h1>Aggiungi partita al calendario</h1>
    <%
        String data = request.getParameter("data");
        String squadra1 = request.getParameter("squadra1");
        String squadra2 = request.getParameter("squadra2");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");

        String user = (String) session.getAttribute("user");
        if (user == null) {
            response.sendRedirect("login.jsp?messaggio=Inserire le credenziali");
        }
      
        out.write("Current logged in user: " + user);
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root","password");
            session.setAttribute("connection", cn);
        }
Statement stmt=cn.createStatement();
        
        //set condition true if all the params are filled
        Boolean conditionNull = data != null && squadra1 != null && squadra2 != null;
        Boolean condition = conditionNull && !(data.equals("") || squadra1.equals("") || squadra2.equals(""));
        DateValidatorUsingDateFormat validator = new DateValidatorUsingDateFormat("dd/MM/yyyy");
        
        //check if the date is valid
        if (condition && !validator.isValid(data)){
            condition = false;
            response.sendRedirect("add-partita.jsp?message=Data formattata in modo errato&messageColor=red");
        }
        
        //check se le squadre esistono
        if (condition){
            Boolean sq1check = false;            
            Boolean sq2check = false;

            ResultSet rsSquadre = stmt.executeQuery("SELECT * FROM squadre");
            while(rsSquadre.next()){
                if (rsSquadre.getString("nome").equals(squadra1))
                    sq1check = true;
                    else if (rsSquadre.getString("nome").equals(squadra2)) {
                        sq2check = true;
                    }
                    
            }
            if (!(sq2check && sq1check)){
                condition = false;
                response.sendRedirect("add-partita.jsp?message=Squadre non presenti&messageColor=red");
            }
        }
        //add partita logic
        if (condition){
            

            String sqlAddPartita = "INSERT INTO partite (dataIncontro, squadra1, squadra2) VALUE ('"+data+"','"+squadra1+"','"+squadra2+"');";
            int result = stmt.executeUpdate(sqlAddPartita);
            response.sendRedirect("add-partita.jsp?message=Partita aggiunta&messageColor=green");
           
         }
         
        if (message == null){
            message = "";
        }
        
            %>
        <form action="add-partita.jsp" method="post">
            Data: <input type="text" name="data"><br>
            Squadra Home: <input type="text" name="squadra1"><br>
            Squadra Guest: <input type="text" name="squadra2"><br>
            <input type="submit" value="Aggiungi">  
        </form>
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>
       

        <br>
        <a href="private.jsp">Ritorna alla home admin</a>
    </body>
</html>
