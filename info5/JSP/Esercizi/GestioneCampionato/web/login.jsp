<%-- 
    Document   : login.jsp
    Created on : 9-nov-2022, 11.23.59
    Author     : federico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <h1>Login admin</h1>
                
        <form action="authorization.jsp" method="post">
            Username: <input type="text" name="user"><br>
            Password: <input type="password" name="pwd"><br><br>
            <input type="submit" value="Login">
            
        </form>
        <%
            String messaggio=request.getParameter("messaggio");
            if(messaggio==null)
                messaggio="";
            if(!messaggio.equals("")){
                out.write("<p style=color:red>"+messaggio+"</p>");
            }
            %>
    </body>
</html>
