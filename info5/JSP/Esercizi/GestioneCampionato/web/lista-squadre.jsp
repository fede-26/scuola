<%-- 
    Document   : lista-squadre
    Created on : Nov 16, 2022, 9:08:39 AM
    Author     : federico
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Elenco squadre</title>
    </head>
    <body>
        <h1>Elenco squadre</h1>
        <%
    Connection cn = (Connection) session.getAttribute("connection");
    if (cn == null) {
        Class.forName("com.mysql.jdbc.Driver");
        cn = DriverManager.getConnection("jdbc:mysql://localhost/campionato", "root", "password");
        session.setAttribute("connection", cn);
    }

    Statement stmt = cn.createStatement();

    String sql = "SELECT * FROM squadre";
    ResultSet rs = stmt.executeQuery(sql);

    out.write("<table border=1px solid grey style='border-collapse: collapse'>");
    out.write("<tr><th>Nome</th><th>Allenatore</th><th>Presidente</th></tr>");
    while (rs.next()) {
        out.write("<tr><td>" + rs.getString("nome") + "</td><td>" + rs.getString("allenatore") + "</td><td>" + rs.getString("presidente") + "</td></tr>" );     
    }
    out.write("</table>");
        %>

        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
