<%-- 
    Document   : logout
    Created on : Nov 16, 2022, 10:08:56 AM
    Author     : federico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body>
        <%
            session.setAttribute("user", null);
            response.sendRedirect("index.html");
            %>
    </body>
</html>
