<%-- 
    Document   : loginù
    Created on : 5-ott-2022, 12.22.16
    Author     : federico.zotti
--%>

<%@page import="java.util.ArrayList" %>
<%@page import="models.User" %>
<%@page import="java.io.File" %>
<%@page import="java.io.IOException" %>
<%@page import="java.nio.file.Files" %>
<%@page import="java.nio.file.Path" %>
<%@page import="java.nio.file.Paths" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <%
            ArrayList<User> users = new ArrayList<User>();
            Path file = Paths.get("users.txt");
// /home/federico/Ship/android-studio/GlassFish_Server/glassfish/domains/domain1/config/users.txt
            String fileContent = Files.readString(file);
            for(String line: fileContent.split("\n")){
                if (line.length() > 0){
                    String[] i = line.split("/");
                    users.add(new User(i[0], i[2], i[1]));
                }
            }
           
//            out.write(file.getCanonicalPath());
            String username=request.getParameter("user");
            String password=request.getParameter("pwd");
            
            User loggedUser = null;
            for (User i: users){
                if (i.getUsername().equals(username)) {
                    if (i.getPwd().equals(password)) {
                        loggedUser = i;
                        session.setAttribute("user", loggedUser);
                    }
                }
            }
            
            if(loggedUser == null){
                response.sendRedirect("form.jsp?messaggio=Credenziali errate");
//                out.write(loggedUser.toString());
            }
            
        %>
        
        <h1>Area riservata <%= loggedUser.getUsername() %></h1>
        
        <a href="settings.jsp" >Entra area per cambiare password</a>
        
    </body>
</html>
