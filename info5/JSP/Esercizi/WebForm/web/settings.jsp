<%-- 
    Document   : loginù
    Created on : 5-ott-2022, 12.22.16
    Author     : federico.zotti
--%>

<%@page import="java.util.ArrayList" %>
<%@page import="models.User" %>
<%@page import="java.io.File" %>
<%@page import="java.io.IOException" %>
<%@page import="java.nio.file.Files" %>
<%@page import="java.nio.file.Path" %>
<%@page import="java.nio.file.Paths" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <%
            String oldPassword=request.getParameter("oldPwd");
            String newPassword=request.getParameter("newPwd");
            
            String message = new String();
            
            User loggedUser = (User) session.getAttribute("user");
            
            if (oldPassword != null && oldPassword.length()>0)
            {
            if (loggedUser.getPwd().equals(oldPassword)){
                //change password
                
                ArrayList<User> users = new ArrayList<User>();
                Path file = Paths.get("users.txt");
// /home/federico/Ship/android-studio/GlassFish_Server/glassfish/domains/domain1/config/users.txt
                String fileContent = Files.readString(file);
                String newFileContent = new String();
                for(String line: fileContent.split("\n")){
                    if (line.length() > 0){
                        if (line.equals(loggedUser.toString())){
                            //change passwd
                            loggedUser.setPwd(newPassword);
                            newFileContent += loggedUser.toString() + "\n";
                        } else {
                            //don't change the line
                            newFileContent += line + "\n";
                        }
                    }
                }
                
                Files.write(file, newFileContent.getBytes());
                message = "Password cambiata: " + oldPassword + " -> " + newPassword;
           

        } else {
        message = "ERRORE: password sbagliata";
        }
            } else {
                //do nothing
        }
            
        %>
        
        <h1>Area riservata <%= loggedUser.getUsername() %></h1>
        
            <form action="settings.jsp" method="post">
            old password: <input type="text" name="oldPwd" ><br>
            new password: <input type="text" name="newPwd" ><br>
            repeat password (doesn't work): <input type="password" name="newPwd2"> <br><br>
            <input type="submit" value="Change">            
            </form>
        
        <%= message%>
        
    </body>
</html>
