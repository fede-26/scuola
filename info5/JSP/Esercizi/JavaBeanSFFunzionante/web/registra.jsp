<%-- 
    Document   : registra
    Created on : 8-feb-2023, 10.24.52
    Author     : giuseppe.depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <jsp:useBean  class="esempio.Persona" id="persona" scope="session" />
        
        <jsp:setProperty name="persona" property="*" />
        <br><br>
        <jsp:getProperty name="persona" property="cognome" />
    </body>
</html>
