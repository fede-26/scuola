/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package esempio;

import java.io.IOException;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 *
 * @author depietro
 */
public class FiltroAdmin implements Filter {

   private FilterConfig filterConfig = null;

        

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        
    }


    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReq=(HttpServletRequest)request;
        HttpServletResponse httpResp=(HttpServletResponse)response;

        //recuperare un oggetto application
        ServletContext application = filterConfig.getServletContext() ;
        //recuperare un oggetto session
        HttpSession session=httpReq.getSession();


        String admin=(String)session.getAttribute("admin");

        
        if (admin==null){
            //String uri=httpReq.getServletPath();
            httpResp.sendRedirect("../nonAutorizzato.jsp");

            //RequestDispatcher dispatcher=servletContext.getRequestDispatcher("/index.html");
            //dispatcher.forward(httpReq,httpResp);

        }
       else {
        chain.doFilter(httpReq, httpResp);
       }
    }

    public void destroy() {
         this.filterConfig = null; 
    }

   
}
