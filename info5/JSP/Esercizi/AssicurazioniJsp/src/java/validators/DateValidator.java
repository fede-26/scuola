package validators;

//preso da qui: https://www.baeldung.com/java-string-valid-date
public interface DateValidator {
   boolean isValid(String dateStr);
}