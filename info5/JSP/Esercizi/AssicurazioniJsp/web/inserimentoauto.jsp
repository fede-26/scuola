<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://latex.now.sh/style.css">

        <title>Inserimento auto</title>
    </head>
    <body>
        <h1>Inserimento auto</h1>
        <%
        String targa = request.getParameter("targa");
        String marca = request.getParameter("marca");
        String modello = request.getParameter("modello");
        String cilindrata = request.getParameter("cilindrata");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");
        if (message == null){
            message = "";
        }
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/assicurazionijsp", "root","password");
            session.setAttribute("connection", cn);
        }

        Statement stmt = cn.createStatement();

        %>


<%

out.write("<h2>Aggiungi auto</h2>");
out.write("<form method='post'>");
out.write("Targa: <input type='text' name='targa'><br>");
out.write("Marca: <input type='text' name='marca'><br>");
out.write("Modello: <input type='text' name='modello'><br>");
out.write("Cilindrata: <input type='number' name='cilindrata'><br>");
out.write("<input type='submit' value='Aggiungi'>");
out.write("</form>");

                    %>
        
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>


        <%
        boolean condizioneNotNull = targa != null && marca != null && modello != null && cilindrata != null;
        boolean condizioneNotVoid = condizioneNotNull && targa != "" && marca != "" && modello != "" && cilindrata != "";
        //questo controllo non funziona
        if (condizioneNotVoid)
        {
            //aggiungi il cliente
            int result = stmt.executeUpdate("INSERT INTO auto (targa, marca, modello, cilindrata) VALUE ('"+targa+"','"+marca+"','"+modello+"','"+cilindrata+"');");
            if (result == 1){
                response.sendRedirect("inserimentoauto.jsp?message=Auto aggiunta&messageColor=green");
            } else {
                response.sendRedirect("inserimentoauto.jsp?message=Errore&messageColor=red");
            }
        }
        
        %>


        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
