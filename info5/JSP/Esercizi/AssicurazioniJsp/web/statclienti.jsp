<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://latex.now.sh/style.css">

        <title>Statistiche clienti</title>
    </head>
    <body>
        <h1>Statistiche clienti</h1>
        <%
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");
        if (message == null){
            message = "";
        }
        
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/assicurazionijsp", "root","password");
            session.setAttribute("connection", cn);
        }

        Statement stmt = cn.createStatement();

        ResultSet pol = stmt.executeQuery("SELECT cliente.*,  polizza.*, auto.*, COUNT(polizza.CodFiscale) AS nPolizze"
                + " FROM polizza "
                + " JOIN auto ON polizza.targa = auto.targa "
                + "JOIN cliente ON polizza.CodFiscale = cliente.CodFiscale "
                + "GROUP BY polizza.CodFiscale");
       
        out.write("<table border=1><tr><th>Cognome</th><th>Nome</th><th>Citta'</th><th>Codice fiscale</th>"
                + "<th>Num. polizze</th></tr>");
        while (pol.next()){
            out.write("<tr>");
            out.write("<td>" + pol.getString("cognome") + "</td>");
            out.write("<td>" + pol.getString("nome") + "</td>");
            out.write("<td>" + pol.getString("citta") + "</td>");
            out.write("<td>" + pol.getString("CodFiscale") + "</td>");
            out.write("<td>" + pol.getString("nPolizze") + "</td>");

//            out.write("<td><form method='post'><input type='hidden' value='"+pol.getString("idPolizza")+"' name='idPol'><input type='submit' value='Interrompi'></form></td>");
            out.write("</tr>");
        }
        out.write("</table>");
        

  

        %>



        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>

        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
