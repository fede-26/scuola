<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://latex.now.sh/style.css">

        <title>Nuova polizza</title>
    </head>
    <body>
        <h1>Nuova polizza</h1>
        <%
        String targa = request.getParameter("autoSel");
        String costoAnnuale = request.getParameter("costoAnnuale");
        String codFiscale = request.getParameter("codFiscale");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");
        if (message == null){
            message = "";
        }
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/assicurazionijsp", "root","password");
            session.setAttribute("connection", cn);
        }

        Statement stmt = cn.createStatement();

        ResultSet clienti = stmt.executeQuery("SELECT * from cliente");
        %>
        <form method="post">
            Cliente: <select name = "codFiscale">
                <%
                        while (clienti.next()) {
                                out.write("<option value=" + clienti.getString("CodFiscale") + ">" + clienti.getString("nome") + " " +clienti.getString("cognome")+  " (" +clienti.getString("CodFiscale")+ ")</option>\n");
                        }
                %>
            </select><br>
            Auto: <select name = "autoSel">
                <%
                        ResultSet automobili = stmt.executeQuery("SELECT * from auto");

                        while (automobili.next()) {
                                out.write("<option value=" + automobili.getString("targa") + ">" + automobili.getString("targa") +" </option>\n");
                        }
                %>
            </select><br>
            Costo annuale: <input type="number" name="costoAnnuale"><br>
            <input type="submit" value="Aggiungi">
        </form>


        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>


        <% 
        boolean condizioneNotNull = targa != null && costoAnnuale != null && codFiscale != null;
        boolean condizioneNotVoid = condizioneNotNull && targa != "" && costoAnnuale != ""  && codFiscale != "";
        //questo controllo non funziona
        if (condizioneNotVoid)
        {
            //aggiungi il cliente
            int result = stmt.executeUpdate("INSERT INTO polizza (targa, CodFiscale, costoAnnuale, DataInizio) VALUE ('"+targa+"','"+codFiscale+"','"+costoAnnuale+"',CURDATE());");
            if (result == 1){
                response.sendRedirect("inserimentopolizza.jsp?message=Polizza aggiunta&messageColor=green");
            } else {
                response.sendRedirect("inserimentopolizza.jsp?message=Errore&messageColor=red");
            }
        }
        
        %>


        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
