<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://latex.now.sh/style.css">

        <title>Inserimento cliente</title>
    </head>
    <body>
        <h1>Inserimento cliente</h1>
        <%
        String nome = request.getParameter("nome");
        String cognome = request.getParameter("cognome");
        String codFiscale = request.getParameter("codFiscale");
        String citta = request.getParameter("citta");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");
        if (message == null){
            message = "";
        }
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/assicurazionijsp", "root","password");
            session.setAttribute("connection", cn);
        }

        Statement stmt = cn.createStatement();


        out.write("<h2>Clienti</h2>");
        ResultSet clienti = stmt.executeQuery("SELECT * FROM cliente");
        out.write("<table border=1><tr><th>Cognome</th><th>Nome</th><th>Citta'</th><th>Codice Fiscale</th></tr>");
        while (clienti.next()){
            out.write("<tr>");
            out.write("<td>" + clienti.getString("cognome") + "</td>");
            out.write("<td>" + clienti.getString("nome") + "</td>");
            out.write("<td>" + clienti.getString("citta") + "</td>");
            out.write("<td>" + clienti.getString("CodFiscale") + "</td>");   
            out.write("</tr>");
        }
        out.write("</table>");

        %>
        <h2>Aggiungi cliente</h2>
        <form method="post">
            Nome: <input type="text" name="nome"><br>
            Cognome: <input type="text" name="cognome"><br>
            Codice Fiscale: <input type="text" name="codFiscale"><br>
            Citta': <input type="text" name="citta"><br>
            <input type="submit" value="Aggiungi">  
        </form>
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>


        <%
        boolean condizioneNotNull = nome != null && cognome != null && codFiscale != null && citta != null;
        boolean condizioneNotVoid = condizioneNotNull && !nome.equals("") && !cognome.equals("") && !codFiscale.equals("") && !citta.equals("");
        if (condizioneNotVoid)
        {
            //aggiungi il cliente
            int result = stmt.executeUpdate("INSERT INTO cliente (nome, cognome, CodFiscale, citta) VALUE ('"+nome+"','"+cognome+"','"+codFiscale+"','"+citta+"');");
            if (result == 1){
                response.sendRedirect("inserimentocliente.jsp?message=Cliente aggiunto&messageColor=green");
            } else {
                response.sendRedirect("inserimentocliente.jsp?message=Errore&messageColor=red");
            }
        }
        
        %>


        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
