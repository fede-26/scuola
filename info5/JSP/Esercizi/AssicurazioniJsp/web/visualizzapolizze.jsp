<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://latex.now.sh/style.css">

        <title>Visualizza polizze attive</title>
    </head>
    <body>
        <h1>Visualizza polizze attive</h1>
        <%
        String idPolInterrompere = request.getParameter("idPol");
        String messageColor = request.getParameter("messageColor");
        String message = request.getParameter("message");
        if (message == null){
            message = "";
        }
        
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/assicurazionijsp", "root","password");
            session.setAttribute("connection", cn);
        }

        Statement stmt = cn.createStatement();

        ResultSet pol = stmt.executeQuery("SELECT cliente.*,  polizza.*, auto.* FROM polizza"
                                            +" JOIN auto ON polizza.targa = auto.targa"
                                            +" JOIN cliente ON polizza.CodFiscale = cliente.CodFiscale"
                                            +" WHERE polizza.DataFine IS NULL");
       
        out.write("<table border=1><tr><th>Cognome</th><th>Nome</th><th>Citta'</th><th>Codice fiscale</th>"
                + "<th>Targa</th><th>Marca</th><th>Modello</th>"
                + "<th>Data inizio</th><th>Costo annuale</th></tr>");
        while (pol.next()){
            out.write("<tr>");
            out.write("<td>" + pol.getString("cognome") + "</td>");
            out.write("<td>" + pol.getString("nome") + "</td>");
            out.write("<td>" + pol.getString("citta") + "</td>");
            out.write("<td>" + pol.getString("CodFiscale") + "</td>");
            out.write("<td>" + pol.getString("targa") + "</td>");
            out.write("<td>" + pol.getString("marca") + "</td>");
            out.write("<td>" + pol.getString("modello") + "</td>");
            out.write("<td>" + pol.getString("DataInizio") + "</td>");
            out.write("<td>" + pol.getString("costoAnnuale") + "</td>");
            out.write("<td><form method='post'><input type='hidden' value='"+pol.getString("idPolizza")+"' name='idPol'><input type='submit' value='Interrompi'></form></td>");
            out.write("</tr>");
        }
        out.write("</table>");
        

        if (idPolInterrompere != null){
            //interrompi polizza
            //aggiungi data fine alla polizza
     
            int result = stmt.executeUpdate("UPDATE polizza"
            + " SET DataFine ="
            + " IF("
            + " DAYOFYEAR(CURRENT_DATE()) < DAYOFYEAR(SUBDATE(DataInizio, 1)),"
            + " MAKEDATE(YEAR(CURRENT_DATE()), DAYOFYEAR(SUBDATE(DataInizio, 1))),"
            + " MAKEDATE(YEAR(CURRENT_DATE())+1,DAYOFYEAR(SUBDATE(DataInizio, 1))))"
            + " WHERE idPolizza = '"+idPolInterrompere+"'");

            if (result != 0) {
            //success
                response.sendRedirect("visualizzapolizze.jsp?message=Polizza interrotta&messageColor=green");
            }else {
                //error
                response.sendRedirect("inserimentopolizza.jsp?message=Errore&messageColor=red");
            }

        }

        %>



        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>

        <br>
        <a href="index.html">Ritorna alla home</a>
    </body>
</html>
