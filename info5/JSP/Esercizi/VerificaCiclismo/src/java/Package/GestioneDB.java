package Package;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matteo
 */
public class GestioneDB 
{
    private Connection cn;
    
    public GestioneDB(String host, String database, String user, String password)
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, user, password);
        } 
        catch (ClassNotFoundException | SQLException ex) 
        {
            Logger.getLogger(GestioneDB.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    public ResultSet select(String sql)
    {
        ResultSet rs = null;
        
        return rs;    
    }
    
    public String selectHTML(String sql)
    {    
        return "";   
    }    
    
    public Connection getCn()
    {
        return cn;
    }
}
