<%-- 
    Document   : signup
    Created on : 30-nov-2022, 10.42.38
    Author     : Matteo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Package.GestioneDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            String user = request.getParameter("user");
            String password = request.getParameter("pwd");
            String nome = request.getParameter("nome");
            String cognome = request.getParameter("cognome");
            
            if (nome.equals("") || cognome.equals("") || user.equals("") || password.equals(""))
                response.sendRedirect("index_signup.jsp?messaggio=compilare-tutti-i-campi");
            else
            {
                String sql = "SELECT * FROM ciclista";
            
                GestioneDB gestione = (GestioneDB)application.getAttribute("gestione");
                if (gestione == null)
                {
                    gestione = new GestioneDB("localhost", "itinerari", "root", "");
                    application.setAttribute("gestione", gestione);
                }

                Connection cn = gestione.getCn();
                Statement stmt = cn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                boolean exists = false;
                while(rs.next())
                {
                    if (rs.getString("username").equals(user))
                        exists = true;
                }

                if (exists)
                    response.sendRedirect("index_signup.jsp?messaggio=utente-esistente");
                else
                {
                    sql = "INSERT INTO ciclista (username, password, cognome, nome) "
                        + "VALUES ('" + user + "', '" + password + "', '" + nome + "', '" + cognome + "');";
                    stmt.executeUpdate(sql);
                    response.sendRedirect("index.jsp?utente-creato");
                }
            }
        %>
    </body>
</html>
