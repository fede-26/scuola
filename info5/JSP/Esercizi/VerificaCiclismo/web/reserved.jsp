<%-- 
    Document   : reserved
    Created on : 30-nov-2022, 10.17.53
    Author     : Matteo
--%>

<%@page import="Package.GestioneDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Reserved </title>
    </head>
    <body>
        <h1 style=color:blue> Reserved </h1>
        <%
            String user = (String)session.getAttribute("user");
            if (user == null)
                user = "";
            if (! user.equals("admin"))
                response.sendRedirect("index.jsp?messaggio=effettuare-login");
        %>
        <form action="logout.jsp" type="post">
            <input type="submit" value="Logout">
        </form> 
        <br> <hr> <br>
        <form action="add_itinerario.jsp" type="post">
            Codice salita: <input type="text" name="cod"> <br>
            Descrizione: <input type="text" name="des"> <br>
            Km totali: <input type="text" name="kmtot"> <br>
            Dislivello: <input type="text" name="dis"> <br><br>
            <input type="submit" value="Aggiungi itinerario">
        </form> 
        <%
            String messaggio = request.getParameter("messaggio");
            if (messaggio == null)
                messaggio = "";
            
            if (messaggio.equals("inserire-tutti-i-campi"))
                out.write("<p style=color:red> Compilare tutti i campi </p>");
            if (messaggio.equals("salita-esistente"))
                out.write("<p style=color:red> Salita già esistente </p>");
            if (messaggio.equals("itinerario-creato"))
                out.write("<p style=color:green> Itinerario creato </p>");
        %>
    </body>
</html>
