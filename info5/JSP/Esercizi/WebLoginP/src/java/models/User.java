/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author federico
 */
public class User {

    public User(String username, String pwd, String surname) {
        this.username = username;
        this.pwd = pwd;
        this.surname = surname;
    }

    public User() {
        this.username = "";
        this.pwd = "";
        this.surname = "";

    }

    String username;
    String pwd;
    String surname;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
   
}
