package models;

/**
 *
 * @author federico
 */
public class User {
    String name;
    String username;
    String surname;
    String email;
    int isAdmin;

    public User(String name, String username, String surname, String email, int isAdmin) {
        this.name = name;
        this.username = username;
        this.surname = surname;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public int getIsAdmin() {
        return isAdmin;
    }
    
    @Override
    public String toString(){
        return name + surname;
    }
}
