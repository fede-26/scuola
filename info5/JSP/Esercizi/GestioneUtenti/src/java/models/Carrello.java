/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author mirko
 */
public class Carrello {
    ArrayList<ProdottoCarrello> contenuto;

    public Carrello() {
        this.contenuto = new ArrayList<ProdottoCarrello>();
    }

    public ArrayList<ProdottoCarrello> getContenuto() {
        return contenuto;
    }

    public void setContenuto(ArrayList<ProdottoCarrello> contenuto) {
        this.contenuto = contenuto;
    }
    
    public ProdottoCarrello getProdottoById(int id){
        ProdottoCarrello aux = null;
        for (ProdottoCarrello x:contenuto){
            if (x.getId() == id){
                aux = x;
                break;
            }
        }       //muraca sta dormendo
        return aux;
    }
    
    public void addProduct(String prodotto){
        int flag = 0;
        for (ProdottoCarrello x:contenuto){
            if (x.getId() == Integer.parseInt(prodotto)){
                x.addOne();
                flag = 1;
                break;
            }
        }       //muraca sta dormendo
        if (flag == 0){
            contenuto.add(new ProdottoCarrello(Integer.parseInt(prodotto), 1));
        }
        
    }
}
