/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mirko
 */
public class ProdottoCarrello {
    int id;
    int quantita;
    
    public ProdottoCarrello(int id, int quantita) {
        this.id = id;
        this.quantita = quantita;
    }

    public int getId() {
        return id;
    }

    public int getQuantita() {
        return quantita;
    }
    
    public void addOne() {
        quantita++;
    }
    
    public void removeOne() {
        quantita--;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }
    
    
}
