<%-- 
    Document   : private
    Created on : Nov 9, 2022, 10:28:16 AM
    Author     : mirko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="models.User"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Private area</title>
    </head>
    <body>
        <h1>Login</h1>
        <%
        String oldPasswd = request.getParameter("oldPasswd");
        String newPasswd = request.getParameter("newPasswd");
        String message = request.getParameter("message");
        String messageColor = request.getParameter("messageColor");
        String changingUser = request.getParameter("userPasswd");


        User user = (User) session.getAttribute("user");
        
        if (changingUser == null){
            changingUser = user.getUsername();
        }
        
        if (user.getIsAdmin() == 0){
            changingUser = user.getUsername();
        }
            out.write(user.toString());
        Connection cn = (Connection) session.getAttribute("connection");
        if (cn == null){
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/gestione_utenti", "root","password");
            session.setAttribute("connection", cn);
        }

        //change password logic
        if (oldPasswd != null && newPasswd != null){
            String sqlGetPasswd = "SELECT * FROM utenti WHERE username='"+changingUser+"' AND password='"+oldPasswd+"'";
            
            Statement stmt=cn.createStatement();
            ResultSet rs=stmt.executeQuery(sqlGetPasswd);
            if (rs.next()){
                String updatePasswd = "UPDATE utenti SET password='"+newPasswd+"' WHERE username='"+changingUser+"'";
                int result = stmt.executeUpdate(updatePasswd);
                response.sendRedirect("private.jsp?message=Password cambiata&messageColor=green");

            } else {
                response.sendRedirect("private.jsp?message=Credenziali errate&messageColor=red");
            }
         }
         
        if (message == null){
            message = "";
        }
        
            %>
        <h1>Changing user:<%= changingUser %></h1>
        <h2>Change password</h2>
        <form action="private.jsp?userPasswd=<%= changingUser %>" method="post">
            Old password: <input type="password" name="oldPasswd"><br>
            New password: <input type="password" name="newPasswd"><br>
            <input type="submit" value="Change">  
        </form>
        <%= "<p style=color:"+messageColor+">"+message+"</p>" %>
        <% if (user.getIsAdmin() == 1){
            out.write("<table border=1>");
            String sql = "SELECT * FROM utenti";
            Statement stmt=cn.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()){
                out.write("<tr><td>"+rs.getString("username")+"</td><td>"+rs.getString("name")+"</td><td>"+rs.getString("surname")+"</td><td>");
                out.write("<form action=\"private.jsp?userPasswd="+rs.getString("username")+"\"><button type=\"submit\">Change Password</button></form></td></tr>");
            }
            out.write("</table");
        }%>
    </body>
</html>