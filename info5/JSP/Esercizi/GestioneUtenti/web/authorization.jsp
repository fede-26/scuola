<%-- 
    Document   : authorization
    Created on : 9-nov-2022, 14.11.34
    Author     : mirko
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String user=request.getParameter("user");
            String password=request.getParameter("pwd");
            String sql="SELECT * FROM utenti WHERE username='"+user+"' AND password='"+password+"'";
            
            Connection cn = (Connection) session.getAttribute("connection");
            if (cn == null){
                Class.forName("com.mysql.jdbc.Driver");
                cn = DriverManager.getConnection("jdbc:mysql://localhost/gestione_utenti", "root","password");
                session.setAttribute("connection", cn);
            }
                    
            java.sql.Statement stmt = cn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            if (rs.next()){
                response.sendRedirect("catalogo.jsp");
                
                User loggedUser = new User(rs.getString("name"), rs.getString("username"), rs.getString("surname"), rs.getString("email"), rs.getInt("isAdmin"));
                
                session.setAttribute("user", loggedUser);
            }else{
                response.sendRedirect("login.jsp?messaggio=Credenziali errate");
            }
        %>
    </body>
</html>
