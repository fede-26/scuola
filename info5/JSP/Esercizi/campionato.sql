-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.1.16-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win32
-- HeidiSQL Versione:            9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database campionato
DROP DATABASE IF EXISTS `campionato`;
CREATE DATABASE IF NOT EXISTS `campionato` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `campionato`;

-- Dump della struttura di tabella campionato.classifica
CREATE TABLE IF NOT EXISTS `classifica` (
  `squadra` varchar(50) NOT NULL,
  `punti` int(11) DEFAULT NULL,
  PRIMARY KEY (`squadra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella campionato.classifica: ~4 rows (circa)
/*!40000 ALTER TABLE `classifica` DISABLE KEYS */;
INSERT INTO `classifica` (`squadra`, `punti`) VALUES
	('Inter', 1),
	('Juventus', 0),
	('Milan', 3),
	('Roma', 1);
/*!40000 ALTER TABLE `classifica` ENABLE KEYS */;

-- Dump della struttura di tabella campionato.partite
CREATE TABLE IF NOT EXISTS `partite` (
  `codPartita` int(11) NOT NULL AUTO_INCREMENT,
  `dataIncontro` varchar(50) NOT NULL DEFAULT '0',
  `squadra1` varchar(50) NOT NULL DEFAULT '0',
  `squadra2` varchar(50) NOT NULL DEFAULT '0',
  `goal1` int(11) NOT NULL DEFAULT '0',
  `goal2` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codPartita`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella campionato.partite: ~2 rows (circa)
/*!40000 ALTER TABLE `partite` DISABLE KEYS */;
INSERT INTO `partite` (`codPartita`, `dataIncontro`, `squadra1`, `squadra2`, `goal1`, `goal2`) VALUES
	(1, '13/11/2022', 'Milan', 'Juventus', 2, 1),
	(2, '13/11/2022', 'Roma', 'Inter', 2, 2);
/*!40000 ALTER TABLE `partite` ENABLE KEYS */;

-- Dump della struttura di tabella campionato.squadre
CREATE TABLE IF NOT EXISTS `squadre` (
  `nome` varchar(50) NOT NULL,
  `allenatore` varchar(50) DEFAULT NULL,
  `presidente` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella campionato.squadre: ~4 rows (circa)
/*!40000 ALTER TABLE `squadre` DISABLE KEYS */;
INSERT INTO `squadre` (`nome`, `allenatore`, `presidente`) VALUES
	('Inter', 'Simone Inzaghi', 'Steven Zhang'),
	('Juventus', 'Massimiliano Allegri', 'Andrea Agnelli'),
	('Milan', 'Stefano Pioli', 'Paolo Scaroni'),
	('Roma', 'José Mourinho', 'Dan Friedkin');
/*!40000 ALTER TABLE `squadre` ENABLE KEYS */;

-- Dump della struttura di tabella campionato.utenti
CREATE TABLE IF NOT EXISTS `utenti` (
  `user` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella campionato.utenti: ~1 rows (circa)
/*!40000 ALTER TABLE `utenti` DISABLE KEYS */;
INSERT INTO `utenti` (`user`, `password`) VALUES
	('admin', 'segreta');
/*!40000 ALTER TABLE `utenti` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
