package customTag.database;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.sql.*;
/**
 * @author depietro
 *
 * 
 */
public class Connetti extends BodyTagSupport {
		private String driver="";
		private String database="";
		private String user="";
		private String password="";
		private Connection cn=null;
		
		/**
		 * @return Restituisce il valore della propriet� cn.
		 */
		public Connection getCn() {
			return cn;
		}
		/**
		 * @param database Imposta il valore della propriet� database.
		 */
                
		public void setDatabase(String database) {
			this.database = database;
		}
                
		/**
		 * @param driver Imposta il valore della propriet� driver.
		 */
		public void setDriver(String driver) {
			this.driver = driver.toUpperCase();
		}
		/**
		 * @param password Imposta il valore della propriet� password.
		 */
		public void setPassword(String password) {
			this.password = password;
		}
		/**
		 * @param user Imposta il valore della propriet� user.
		 */
		public void setUser(String user) {
			this.user = user;
		}
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.SimpleTag#doTag()
	 */
	public int doStartTag() throws JspException {
		// TODO doTag()
		String sdc=""; 			//stringa di connessione
		String clsDriver="";	//class for name
		if(driver.equals("MYSQL")){
			clsDriver="com.mysql.jdbc.Driver";
			sdc="jdbc:mysql://";
		}
		if(driver.equals("SQL2K")){
			clsDriver="com.microsoft.jdbc.sqlserver.SQLServerDriver";
			sdc="jdbc:microsoft:sqlserver://";
		}
		if(driver.equals("ORACL")){
			//
		}	
		sdc+=database+"?user="+user+"&password="+password;
		
		try {
			Class.forName(clsDriver);
			cn = DriverManager.getConnection(sdc);
		} catch (SQLException e) {
			// TODO SQLException
			throw new JspException ("Errore connessione."+e.getMessage());
		}catch (Exception e) {
			throw new JspException ("Connetti.doStartTag. Errore:"+e.getMessage());
		} 
		return EVAL_BODY_BUFFERED; 
	}
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try {
			// TODO doEndTag()
			cn.close();
			JspWriter out=pageContext.getOut();
			//recupero l'output generato nei tag annidati
			out.print(bodyContent.getString());
		} catch (Exception e) {
			throw new JspException ("Si e' verificato il seguente errore: "+e.getMessage());
		}
		
		return EVAL_PAGE;
	}
} //fine classe
