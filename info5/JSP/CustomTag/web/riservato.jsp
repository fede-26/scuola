<%-- 
    Document   : riservato
    Created on : 2-feb-2018, 8.20.11
    Author     : depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Area riservata</h1>
        <%
            String user=(String)session.getAttribute("user");
            if (user==null)
                user="";
            if (!user.equals("admin"))
                response.sendRedirect("index.jsp");
         %>
    </body>
</html>
