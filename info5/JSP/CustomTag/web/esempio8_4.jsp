<%@ page language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="IT">
<head>
<title>Custom Tag con combobox dinamico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<h2>Creazione di un Custom Tag con combobox dinamico</h2>
<p>Il custom tag si connette ad un database e crea un combobox (controllo SELECT) con gli elementi di una tabella fornita come attributo del tag <i>combo</i></p>   

<%@ taglib uri="/WEB-INF/tlds/dpDatabase.tld" prefix="dpDatabase"%> 
       
     
<dpDatabase:connetti database="localhost/appartamento" driver="MYSQL" user="root" password="">
    <dpDatabase:combo nome="proprietari" tabella="proprietari" text="cognome" value="codProprietario" />
</dpDatabase:connetti>
        
            
            
       
	
	
        
</body>
</html>
