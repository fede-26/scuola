<%-- 
    Document   : esempioTag2
    Created on : 9-feb-2018, 9.22.41
    Author     : depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="WEB-INF/tlds/customTag.tld" prefix="dp"%> 
<!DOCTYPE html>
<html>
    <head>
     <title>Creare dei Custom Tag con attributi</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<h2>Creazione di un Custom Tag con attributi</h2>
<form method="post" >
	<fieldset>
		<legend>Nome utente che verrà aggiunto al messaggio</legend>
		<label for="nome">Inserisci il nome: </label>
		<input type="text" name="nome" id="nome">
		<br/><br/>
		<input type="submit" value="Login" name="invia">
	</fieldset>
</form>
<%
if (request.getParameter("invia")!=null){
	%>
	 
        <dp:SecondoTag  nome='<%=request.getParameter("nome")%>'/>
<%}%>
</html>
