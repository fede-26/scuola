<%-- 
    Document   : newjsp
    Created on : 15-feb-2023, 10.18.14
    Author     : giuseppe.depietro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib uri="WEB-INF/tlds/customTag.tld" prefix="dpTag"%>
    
    <dpTag:PrimoTag />
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
