<%-- 
    Document   : index
    Created on : 30-nov-2022, 10.10.38
    Author     : Matteo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Itinerari </title>
    </head>
    <body>
        <h1 style=color:blue> Itinerari </h1>
        <form action="login.jsp" method="post">
            Username: <input type="text" name="user"> <br>
            Password: <input type="password" name="pwd"> <br><br>
            <input type="submit" value="Login">
        </form>
        <form action="index_signup.jsp" method="post">
            <input type="submit" value="Sign up">
        </form>
        <%
            String messaggio=request.getParameter("messaggio");
            if (messaggio == null)
                messaggio = "";
            
            if (messaggio.equals("user-o-password-errati"))
                out.write("<p style=color:red> Username o password errati </p>");
            if (messaggio.equals("effettuare-login"))
                out.write("<p style=color:red> Effettuare il login </p>");
            if (messaggio.equals("utente-creato"))
                out.write("<p style=color:green> Utente creato </p>");
        %>
    </body>
</html>
