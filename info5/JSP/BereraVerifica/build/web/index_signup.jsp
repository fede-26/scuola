<%-- 
    Document   : index_signup
    Created on : 30-nov-2022, 10.39.31
    Author     : Matteo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Sign up </title>
    </head>
    <body>
        <h1 style=color:blue> Sign up </h1>
        <form action="signup.jsp" method="post">
            Nome: <input type="text" name="nome"> <br>
            Cognome: <input type="text" name="cognome"> <br>
            Username: <input type="text" name="user"> <br>
            Password: <input type="password" name="pwd"> <br><br>
            <input type="submit" value="Sign up">
        </form> 
        <%
            String messaggio=request.getParameter("messaggio");
            if (messaggio == null)
                messaggio = "";
            
            if (messaggio.equals("compilare-tutti-i-campi"))
                out.write("<p style=color:red> Compilare tutti i campi </p>");
            if (messaggio.equals("utente-esistente"))
                out.write("<p style=color:red> Utente già esistente </p>");
        %>
    </body>
</html>
