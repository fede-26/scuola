<%-- 
    Document   : add_tempo
    Created on : 30-nov-2022, 11.02.03
    Author     : Matteo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Package.GestioneDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            String user = (String)session.getAttribute("user");
            if (user == null)
                user = "";
            if (user.equals(""))
                response.sendRedirect("index.jsp?messaggio=effettuare-login");
            
            String salita = request.getParameter("salita");
            String hh = request.getParameter("hh");
            String mm = request.getParameter("mm");
            String ss = request.getParameter("ss");

            if (salita.equals("") || mm.equals("") || ss.equals(""))
                response.sendRedirect("public.jsp?messaggio=inserire-tutti-i-campi");
            else
            {
                GestioneDB gestione = (GestioneDB)application.getAttribute("gestione");
                if (gestione == null)
                {
                    gestione = new GestioneDB("localhost", "itinerari", "root", "");
                    application.setAttribute("gestione", gestione);
                }

                Connection cn = gestione.getCn();
                Statement stmt = cn.createStatement();
                String sql = "SELECT * FROM tempi";
                ResultSet rs = stmt.executeQuery(sql);
                
                int lastTempo = 0;
                while(rs.next())
                    lastTempo = rs.getInt("idTempo");
                
                if (hh.equals(""))
                    hh = "00";
                if (hh.length() == 1)
                    hh = "0" + hh;
                if (mm.length() == 1)
                    mm = "0" + mm;
                if (ss.length() == 1)
                    ss = "0" + ss;
                String tempo = hh + ":" + mm + ":" + ss;
                
                sql = "INSERT INTO tempi (idTempo, username, codSalita, tempo) "
                        + "VALUES (" + (lastTempo+1) + ", '" + session.getAttribute("user") + "', '" + salita + "', '" + tempo + "');";
                stmt.executeUpdate(sql);
                
                response.sendRedirect("public.jsp?messaggio=tempo-aggiunto");
            }
        %>
    </body>
</html>
