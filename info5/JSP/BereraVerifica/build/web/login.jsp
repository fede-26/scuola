<%-- 
    Document   : login
    Created on : 11-ott-2022, 11.22.32
    Author     : Matteo
--%>

<%@page import="Package.GestioneDB"%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            String user = request.getParameter("user");
            String password = request.getParameter("pwd");
            String sql = "SELECT * FROM ciclista WHERE username='" + user + "' AND password='" + password + "'";
            
            GestioneDB gestione = (GestioneDB)application.getAttribute("gestione");
            if (gestione == null)
            {
                gestione = new GestioneDB("localhost", "itinerari", "root", "");
                application.setAttribute("gestione", gestione);
            }
            
            Connection cn = gestione.getCn();
            Statement stmt = cn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next())
            {
                if (rs.getString("username").equals("admin"))
                    response.sendRedirect("reserved.jsp");
                else
                    response.sendRedirect("public.jsp");
                session.removeAttribute("user");
                session.setAttribute("user", user);
            }
            else
                response.sendRedirect("index.jsp?messaggio=user-o-password-errati");
        %>
    </body>
</html>
