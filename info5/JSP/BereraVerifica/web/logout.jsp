<%-- 
    Document   : logout
    Created on : 30-nov-2022, 11.39.20
    Author     : Matteo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            session.removeAttribute("user");
            response.sendRedirect("index.jsp");
        %>
    </body>
</html>
