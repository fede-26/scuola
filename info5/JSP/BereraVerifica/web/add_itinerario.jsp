<%-- 
    Document   : add_itinerario
    Created on : 30-nov-2022, 10.21.49
    Author     : Matteo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Package.GestioneDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            String user = (String)session.getAttribute("user");
            if (user == null)
                user = "";
            if (user.equals(""))
                response.sendRedirect("index.jsp?messaggio=effettuare-login");
            
            String codice = request.getParameter("cod");
            String descrizione = request.getParameter("des");
            String kmTotali = request.getParameter("kmtot");
            String dislivello = request.getParameter("dis");
            
            if (codice.equals("") || descrizione.equals("") || kmTotali.equals("") || dislivello.equals(""))
                response.sendRedirect("reserved.jsp?messaggio=inserire-tutti-i-campi");
            else
            {
                String sql = "SELECT * FROM salite";
            
                GestioneDB gestione = (GestioneDB)application.getAttribute("gestione");
                if (gestione == null)
                {
                    gestione = new GestioneDB("localhost", "itinerari", "root", "");
                    application.setAttribute("gestione", gestione);
                }

                Connection cn = gestione.getCn();
                Statement stmt = cn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                boolean exists = false;
                while(rs.next())
                {
                    if (rs.getString("codSalita").equals(codice))
                        exists = true;
                }

                if (exists)
                    response.sendRedirect("reserved.jsp?messaggio=salita-esistente");
                else
                {
                    sql = "INSERT INTO salite (codSalita, nome, lunghezza, dislivello) "
                        + "VALUES ('" + codice + "', '" + descrizione + "', " + kmTotali + ", " + dislivello + ");";
                    stmt.executeUpdate(sql);
                    response.sendRedirect("reserved.jsp?messaggio=itinerario-creato");
                }
            }
        %>
    </body>
</html>
