<%-- 
    Document   : public
    Created on : 30-nov-2022, 10.56.38
    Author     : Matteo
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Package.GestioneDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Itinerari </title>
    </head>
    <body>
        <%
            String user = (String)session.getAttribute("user");
            if (user == null)
                user = "";
            if (user.equals(""))
                response.sendRedirect("index.jsp?messaggio=effettuare-login");
            
            out.write("<h1 style=color:blue> Benvenuto " + session.getAttribute("user") + "</h1>");
        %>
        <form action="logout.jsp" type="post">
            <input type="submit" value="Logout">
        </form> 
        <br> <hr> <br>
        <form action="add_tempo.jsp" method="post">
            <select name="salita">
                <%
                    GestioneDB gestione = (GestioneDB)application.getAttribute("gestione");
                    if (gestione == null)
                    {
                        gestione = new GestioneDB("localhost", "itinerari", "root", "");
                        application.setAttribute("gestione", gestione);
                    }
            
                    String sql = "SELECT * FROM salite";

                    Connection cn = gestione.getCn();
                    Statement stmt = cn.createStatement();
                    ResultSet rs = stmt.executeQuery(sql);
                    
                    while(rs.next())
                        out.write("<option value='" + rs.getString("codSalita") + "'>" + rs.getString("nome") + "</option>");
                %>
            </select> <br>
            Tempo hh: <input type="text" name="hh"> <br>
            Tempo mm: <input type="text" name="mm"> <br>
            Tempo ss: <input type="text" name="ss"> <br><br>
            <input type="submit" value="Aggiungi tempo">
        </form>
        <%
            String messaggio = request.getParameter("messaggio");
            if (messaggio == null)
                messaggio = "";
            
            if (messaggio.equals("inserire-tutti-i-campi"))
                out.write("<p style=color:red> Inserire tutti i campi </p>");
            if (messaggio.equals("tempo-aggiunto"))
                out.write("<p style=color:green> Tempo aggiunto correttamente </p>");
        %>
        <hr> <br>
            <%
                sql = "SELECT * FROM salite";
                rs = stmt.executeQuery(sql);
                    
                while (rs.next())
                { 
                    sql = "SELECT * FROM tempi WHERE codSalita='" + rs.getString("codSalita") + "' ORDER BY tempo";
                    Statement stmt2 = cn.createStatement();
                    ResultSet rs2 = stmt2.executeQuery(sql);
                    
                    DecimalFormat df = new DecimalFormat();
                    df.setMaximumFractionDigits(2);

                    int pos = 1;
                    while (rs2.next())
                    {
                        String[] t = rs2.getString("tempo").split(":");
                        int tempoSec = Integer.valueOf(t[0]) * 3600 + Integer.valueOf(t[1]) * 60 + Integer.valueOf(t[2]);
                        if (pos == 1)
                        {
                            out.write("<table border='1'>" +
                                    "<tr> <th colspan='5'>" + rs.getString("nome") + "</th> </tr>" +
                                    "<tr> <th> Pos. </th> <th> Utente </th> <th> Tempo </th> <th> VM </th> <th> VAM </th> </tr>");
                            out.write("<tr bgcolor='yellow'> <td> " + pos + "</td>" + 
                                    "<td>" + rs2.getString("username") + "</td>" + 
                                    "<td>" + rs2.getString("tempo") + "</td>" + 
                                    "<td>" + df.format(Float.valueOf(rs.getInt("lunghezza")) * 3600 / Float.valueOf(tempoSec)) + " km/h</td>" + 
                                    "<td>" + df.format(Float.valueOf(rs.getInt("dislivello")) * 3600 / Float.valueOf(tempoSec)) + " m/h</td> </tr>");
                        }
                        else
                            out.write("<tr> <td> " + pos + "</td>" + 
                                    "<td>" + rs2.getString("username") + "</td>" + 
                                    "<td>" + rs2.getString("tempo") + "</td>" + 
                                    "<td>" + df.format(Float.valueOf(rs.getInt("lunghezza")) * 3600 / Float.valueOf(tempoSec)) + " km/h</td>" + 
                                    "<td>" + df.format(Float.valueOf(rs.getInt("dislivello")) * 3600 / Float.valueOf(tempoSec)) + " m/h</td> </tr>");
                        pos += 1;
                    }
                    out.write("</table> <br>"); 
                }
            %>
        </table>
    </body>
</html>
