//! # rsa_utils
//! 
//! `rsa_utils` è una libreria che contiene funzioni comuni utilizzate nell'algoritmo RSA.
//! Può essere importata in altri crates per utilizzare le funzioni.

/// Genera un gruppo moltiplicativo modulo n.
/// 
/// # Esempi
/// 
/// ```
/// let n = 10;
/// let group = rsa_utils::generate_group(n);
/// assert_eq!(group, Ok(vec![1, 3, 7, 9]));
/// ```
/// 
/// # Errori
/// 
/// Se il numero è negativo, viene restituito un errore.
pub fn generate_group(n: i64) -> Result<Vec<i64>, String> {
    if n <= 0 {
        return Err("The number must be positive".to_string());
    }

    let mut group: Vec<i64> = Vec::new();
    for i in 1..n {
        if gcd(i, n)? == 1 {
            group.push(i);
        }
    }
    Ok(group)
}

/// Calcola il phi di un numero.
/// 
/// [it.wikipedia.org/wiki/Funzione_%CF%86_di_Eulero](https://it.wikipedia.org/wiki/Funzione_%CF%86_di_Eulero)
/// 
/// # Esempi
/// 
/// ```
/// let n = 10;
/// let x = rsa_utils::phi(n);
/// assert_eq!(x, Ok(4));
/// ```
/// 
/// # Errori
/// 
/// Se il numero è negativo, viene restituito un errore.
pub fn phi(n: i64) -> Result<i64, String> {
    if n <= 0 {
        return Err("The number must be positive".to_string());
    }

    let mut count = 0;
    for i in 1..n {
        if gcd(i, n)? == 1 {
            count += 1;
        }
    }
    Ok(count)
}

/// Funzione che calcola il massimo comun divisore tra due numeri.
/// Utilizza l'algoritmo di Euclide.
/// 
/// # Esempi
/// 
/// ```
/// let a = 10;
/// let b = 5;
/// let x = rsa_utils::gcd(a, b);
/// assert_eq!(x, Ok(5));
/// ```
/// 
/// # Errori
/// 
/// Se `a <= 0` o `b < 0`, viene restituito un errore.
pub fn gcd(a: i64, b: i64) -> Result<i64, String> {
    if a <= 0 || b < 0 {
        return Err("The numbers must be positive".to_string());
    }

    if b == 0 {
        Ok(a)
    } else {
        gcd(b, a % b)
    }
}


/// Funzione che calcola il massimo comun divisore e i numeri per l'identità di Bézout.
/// Utilizza l'algoritmo di Euclide esteso.
/// 
/// # Return
/// 
/// Restituisce una tupla con il massimo comun divisore e i numeri per l'identità di Bézout.
/// 
/// # Esempi

/// ```
/// let a = 10;
/// let b = 5;
/// let (gcd, x, y) = rsa_utils::egcd(a, b).unwrap();
/// assert_eq!(gcd, 5);
/// assert_eq!(x, 0);
/// assert_eq!(y, 1);
/// ```

pub fn egcd(a: i64, b: i64) -> Result<(i64, i64, i64), String> {

    if a < 0 || b < 0 {
        return Err("The numbers must be positive".to_string());
    }

    let mut a = a;
    let mut b = b;
    let mut x0 = 0;
    let mut x1 = 1;
    let mut y0 = 1;
    let mut y1 = 0;
    let mut q;

    while a != 0 {
        (q, a, b) = (b / a, b % a, a);
        (y0, y1) = (y1, y0 - q * y1);
        (x0, x1) = (x1, x0 - q * x1);
    }

    Ok((b, x0, y0))

}



/// Funzione che calcola il numero inverso in un gruppo moltiplicativo modulo n.
/// 
/// Al momento non è stata implementata.
/// 
/// # Esempi
/// 
/// ```
/// let n = 11;
/// let a = 3;
/// let x = rsa_utils::inverse(n, a);
/// assert_eq!(x, Ok(4));
/// ```
/// 
/// # Errori
/// 
/// Restituisce sempre un errore perchè non è implementata.
pub fn inverse(n: i64, a: i64) -> Result<i64, String> {
    if a <= 0 || n <= 0 {
        return Err("The numbers must be positive".to_string());
    }

    //TODO: vedere se questo è necessario
    if a > n {
        return Err("The number must be less than the mod".to_string());
    }

    let (gcd, x, y) = egcd(a, n)?;

    if gcd != 1 {
        return Err("The numbers must be coprime".to_string());
    }

    Ok(x)

}

//test for all the functions
#[cfg(test)]
mod tests {
    use super::*;

    //test for the group generation
    #[test]
    fn group_of_n() {
        assert_eq!(generate_group(10), Ok(vec![1, 3, 7, 9]));
    }

    //test for phi
    #[test]
    fn phi_of_n() {
        assert_eq!(phi(10), Ok(4));
    }

    //test for gcd
    #[test]
    fn gcd_of_two_numbers() {
        assert_eq!(gcd(10, 5), Ok(5));
    }

    //test for inverse
    #[test]
    fn reverse_of_a_number() {
        // assert_eq!(reverse(10, 5), Ok(5));
        assert_eq!(inverse(20, 7), Ok(3));
        assert_eq!(inverse(20, 3), Ok(7));
    }

    //test for egcd
    #[test]
    fn egcd_of_two_numbers() {
        assert_eq!(egcd(10, 5), Ok((5, 0, 1)));
        assert_eq!(egcd(20, 7), Ok((1, -1, 3)));
    }
}
