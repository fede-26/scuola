# Progettazione db

3 tabelle:

- alunno
- voto
- esito

## alunno

- id_alunno / matricola (incrementale/casuale)
- nome
- cognome
- num_registro?
- classe
- indirizzo

## voto

ci sono tutte le materie, quelle che lo studente non frequenta sono NULL

- id_alunno
- nome_materia
- voto

## esito

- id_alunno
- esito