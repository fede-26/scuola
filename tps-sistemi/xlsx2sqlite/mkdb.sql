BEGIN TRANSACTION;
DROP TABLE IF EXISTS "alunno";
CREATE TABLE IF NOT EXISTS "alunno" (
	"id"	TEXT NOT NULL,
	"nome"	TEXT,
	"classe"	TEXT,
	"indirizzo"	TEXT,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "voto";
CREATE TABLE IF NOT EXISTS "voto" (
	"id_alunno"	TEXT,
	"materia"	TEXT,
	"voto"	TEXT,
	PRIMARY KEY("materia","voto","id_alunno"),
	FOREIGN KEY("id_alunno") REFERENCES "alunno"("id")
);
DROP TABLE IF EXISTS "esito";
CREATE TABLE IF NOT EXISTS "esito" (
	"id_alunno"	TEXT,
	"esito"	TEXT,
	FOREIGN KEY("id_alunno") REFERENCES "alunno"("id"),
	PRIMARY KEY("id_alunno")
);
COMMIT;
