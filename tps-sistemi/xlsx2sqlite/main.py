#/usr/bin/env python3

# Programma che stampa un file xls

import xlrd
import sqlite3
import hashlib
import sys

file = sys.argv[1]
# Apertura file
workbook = xlrd.open_workbook(file)

# Stampa foglio
print(f"Foglio: {workbook.sheet_names()[0]}")
# print()

# Trova la cella che contiene la stringa che contine la sottostringa 'IST' nella seconda riga
worksheet = workbook.sheet_by_name(workbook.sheet_names()[0])
for row in range(1, worksheet.nrows):
    for col in range(worksheet.ncols):
        if 'IST' in worksheet.cell_value(row, col):
            # TODO: fare il parsing della stringa per trovare la classe
            classeEisti = worksheet.cell_value(row, col)
            print(f"Classe: [{row}, {col}] -> '{classeEisti}'")
            break

classe = classeEisti.split(' ')[0]
indirizzo = ' '.join(classeEisti.split(' ')[1:])
# Identifica indice della riga con la prima cella 'Pr.'
prima_riga = worksheet.col_values(0).index('Pr.')
print(f"Riga con la lista delle materie: {prima_riga}")

# Stampa righe del foglio 'TABELLONE VOTI' a partire dalla riga 'Pr.'
# worksheet = workbook.sheet_by_name(workbook.sheet_names()[0])
# for row in range(prima_riga, worksheet.nrows):
#     print(worksheet.row_values(row))

# Trova la seconda riga contenente solo stringhe vuote
for row in range(prima_riga, worksheet.nrows):
    if all(isinstance(cell, str) and cell == '' for cell in worksheet.row_values(row)):
        seconda_riga = row
        break

print(f"Seconda riga vuota: {seconda_riga}")

# Associa i voti con le materie con zip
materie = worksheet.row_values(prima_riga)

# Apre una connessione al db
conn = sqlite3.connect('/home/federico/programs/scuola/tps-sistemi/xlsx2sqlite/voti.db')
c = conn.cursor()


# For tutti gli studenti prima_riga+1 -> seconda_riga-1
for studente in range(prima_riga+1, seconda_riga):
    voti = worksheet.row_values(studente)
    # print(f"Studente {voti}:")
    voti_studente = list(zip(materie, voti))
    # Rimuove le tuple vuote
    voti_studente = [voto for voto in voti_studente if voto[1] != '']

    # Trova la tupla con la stringa 'Alunno'
    # Salva l'indice della colonna e il nome dello studente        
    for i, voto in enumerate(voti_studente):
        if 'Alunno' in voto[0]:
            print(f"Studente: {voto[1]}")
            # Al momento non gestisce i secondi nomi e i cognomi con spazi
            # cognome, nome = voto[1].split(' ')
            nome = voto[1]
            col_alunno = i
            break

    # id è un hash md5 degli altri attributi
    id = hashlib.md5(f"{nome}{classe}{indirizzo}".encode('utf-8')).hexdigest()
    # Inserisce lo studente nel db
    c.execute("INSERT INTO alunno (id, nome, classe, indirizzo) VALUES (?,?,?,?)", (id, nome, classe, indirizzo))
    print(c.lastrowid)
    conn.commit()


    # TODO: aggiungere voti e esito nel db
    print(f"{voti_studente}")
    for i, voto in enumerate(voti_studente):
        if i <= col_alunno:
            continue
        if voto[0].lower() == 'media':
            continue
        if voto[0].lower() == 'esito':
            c.execute("INSERT INTO esito (id_alunno, esito) VALUES (?,?)", (id, voto[1]))
            conn.commit()
            break
        # if voto[0].isupper():
        c.execute("INSERT INTO voto (id_alunno, materia, voto) VALUES (?,?,?)", (id, voto[0], voto[1]))
        conn.commit()


# Chiude la connessione al db
conn.close()