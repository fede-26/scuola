BEGIN TRANSACTION;
DROP TABLE IF EXISTS "studente";
CREATE TABLE IF NOT EXISTS "studente" (
	"id_studente"	INTEGER,
	"nome"	TEXT,
	"cognome"	TEXT,
	"classe"	INTEGER,
	"indirizzo"	TEXT,
	PRIMARY KEY("id_studente" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "voto";
CREATE TABLE IF NOT EXISTS "voto" (
	"id_voto"	INTEGER,
	"id_studente"	INTEGER,
	"nome_materia"	TEXT NOT NULL,
	"voto"	INTEGER,
	PRIMARY KEY("id_voto" AUTOINCREMENT),
	FOREIGN KEY("id_studente") REFERENCES "studente"("id_studente")
);
DROP TABLE IF EXISTS "esito";
CREATE TABLE IF NOT EXISTS "esito" (
	"id_studente"	INTEGER,
	"esito"	TEXT,
	PRIMARY KEY("id_studente"),
	FOREIGN KEY("id_studente") REFERENCES "studente"("id_studente")
);
INSERT INTO "studente" VALUES (1,'Dennis','Corabi',5,'Informatica');
INSERT INTO "voto" VALUES (1,1,'Italiano',7);
INSERT INTO "voto" VALUES (2,1,'Inglese',5);
INSERT INTO "esito" VALUES (1,'Ammesso');
COMMIT;
