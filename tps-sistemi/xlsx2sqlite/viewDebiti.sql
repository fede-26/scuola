select alunno.nome, alunno.classe, voto.materia, voto.voto
from alunno
join voto on voto.id_alunno = alunno.id
join esito on esito.id_alunno = alunno.id
where (esito is 'Ammesso/a' or esito is 'Sospensione del giudizio') and voto < 6 and not voto.materia = 'RELIGIONE'
ORDER by alunno.nome