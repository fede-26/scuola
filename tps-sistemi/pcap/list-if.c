#include <stdio.h>
#include <pcap.h>

int main(int argc, char **argv) {
    pcap_if_t *device; /* Device (e.g. eth0, wlan0) */
    char error_buffer[PCAP_ERRBUF_SIZE]; /* Size defined in pcap.h */

    /* Find a device */
    pcap_findalldevs(&device, error_buffer);
    if (device == NULL) {
        printf("Error finding device: %s\n", error_buffer);
        return 1;
    }

    //Print devices
    pcap_if_t *print_device = device;
    while (device != NULL){
        printf("Network device found: %s\n", device->name);
        device = device->next;
    }
    return 0;
}