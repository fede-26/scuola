#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Programma che fa il parsing di un file chiave ssh e ne stampa il contenuto diviso in più parti

import base64


def open_key():
    with open('key') as f:
        content = f.read()

    base64EncodedStr = base64.b64decode(content)
    print(base64EncodedStr)

if __name__ == '__main__':
    open_key()