```python
with open("energy.cod", "rb") as f:
    content = list(f.read())
```

# Analisi delle frequenze

## Frequenze assolute


```python
#conta le lettere
all_freq = list(range(256))
for i in range(256):
    all_freq[i] = [i, content.count(i)]

#ordina le frequenze per valore e non per chiave
all_freq.sort(key=lambda x: x[1])
all_freq.reverse()

#print
print("--- FREQUENZE ASSOLUTE ---")
for i in range(256):
    if all_freq[i][1] > 0:
        print("{}\t-> {}".format(repr(chr(all_freq[i][0])), all_freq[i][1]))
print()
```

    --- FREQUENZE ASSOLUTE ---
    '}'	-> 1784
    'á'	-> 1209
    'Z'	-> 1109
    '\x00'	-> 966
    'ø'	-> 900
    '<'	-> 725
    ','	-> 680
    '«'	-> 651
    'Ê'	-> 574
    'D'	-> 423
    '/'	-> 379
    'ÿ'	-> 333
    'r'	-> 279
    'o'	-> 266
    '½'	-> 242
    'Ì'	-> 189
    '2'	-> 158
    'l'	-> 124
    'â'	-> 123
    '\x1d'	-> 85
    '©'	-> 81
    '4'	-> 77
    ')'	-> 67
    '\x1e'	-> 65
    '»'	-> 46
    '¦'	-> 45
    '@'	-> 45
    '0'	-> 45
    '5'	-> 43
    'þ'	-> 38
    'N'	-> 27
    'M'	-> 27
    '#'	-> 24
    '\x05'	-> 24
    '%'	-> 22
    'Ñ'	-> 20
    'k'	-> 20
    'ü'	-> 19
    'Û'	-> 16
    '`'	-> 16
    'Ã'	-> 15
    ' '	-> 14
    'Ë'	-> 13
    'ã'	-> 12
    'Å'	-> 12
    'K'	-> 12
    'z'	-> 10
    'J'	-> 10
    'Ö'	-> 9
    '\x92'	-> 9
    'ù'	-> 8
    '\x85'	-> 8
    '\x19'	-> 8
    '\x0b'	-> 8
    '×'	-> 7
    '´'	-> 7
    '\x9e'	-> 7
    'W'	-> 7
    'ñ'	-> 6
    '\x88'	-> 6
    '\x82'	-> 6
    'Þ'	-> 5
    '\x96'	-> 5
    '.'	-> 5
    'õ'	-> 4
    '"'	-> 4
    'ð'	-> 3
    '¹'	-> 3
    '¥'	-> 3
    '¤'	-> 3
    '\x89'	-> 3
    '~'	-> 3
    'i'	-> 3
    '¿'	-> 2
    '¬'	-> 2
    'g'	-> 2
    '\x87'	-> 1
    '3'	-> 1
    '\x1f'	-> 1
    '\x10'	-> 1
    


## Frequenze relative


```python
#calcola le frequenze relative
rel_freq = list()
for letter, freq in all_freq:
    if freq > 0:
        rel_freq.append((letter, freq/len(content)))

#print
print("--- FREQUENZE RELATIVE ---")
for i, j in rel_freq:
    print("{}: {}".format(repr(i), j))
```

    --- FREQUENZE RELATIVE ---
    125: 0.14606189618470608
    225: 0.09898477157360407
    90: 0.09079744555428197
    0: 0.07908956934665139
    248: 0.0736859341738988
    60: 0.05935811364008515
    44: 0.05567381693139021
    171: 0.0532994923857868
    202: 0.04699525135090879
    68: 0.03463238906173244
    47: 0.03102996561323072
    255: 0.02726379564434256
    114: 0.02284263959390863
    111: 0.021778287211396758
    189: 0.019813328966759455
    204: 0.01547404617651875
    50: 0.0129359751105289
    108: 0.01015228426395939
    226: 0.01007041100376617
    29: 0.006959227116423776
    169: 0.006631734075650892
    52: 0.006304241034878009
    41: 0.0054855084329458
    30: 0.005321761912559358
    187: 0.0037661699688881613
    166: 0.00368429670869494
    64: 0.00368429670869494
    48: 0.00368429670869494
    53: 0.0035205501883084985
    254: 0.003111183887342394
    78: 0.0022105780252169643
    77: 0.0022105780252169643
    35: 0.0019649582446373015
    5: 0.0019649582446373015
    37: 0.0018012117242508596
    209: 0.0016374652038644178
    107: 0.0016374652038644178
    252: 0.001555591943671197
    219: 0.0013099721630915344
    96: 0.0013099721630915344
    195: 0.0012280989028983134
    32: 0.0011462256427050926
    203: 0.0010643523825118715
    227: 0.0009824791223186507
    197: 0.0009824791223186507
    75: 0.0009824791223186507
    122: 0.0008187326019322089
    74: 0.0008187326019322089
    214: 0.000736859341738988
    146: 0.000736859341738988
    249: 0.0006549860815457672
    133: 0.0006549860815457672
    25: 0.0006549860815457672
    11: 0.0006549860815457672
    215: 0.0005731128213525463
    180: 0.0005731128213525463
    158: 0.0005731128213525463
    87: 0.0005731128213525463
    241: 0.0004912395611593254
    136: 0.0004912395611593254
    130: 0.0004912395611593254
    222: 0.00040936630096610445
    150: 0.00040936630096610445
    46: 0.00040936630096610445
    245: 0.0003274930407728836
    34: 0.0003274930407728836
    240: 0.0002456197805796627
    185: 0.0002456197805796627
    165: 0.0002456197805796627
    164: 0.0002456197805796627
    137: 0.0002456197805796627
    126: 0.0002456197805796627
    105: 0.0002456197805796627
    191: 0.0001637465203864418
    172: 0.0001637465203864418
    103: 0.0001637465203864418
    135: 8.18732601932209e-05
    51: 8.18732601932209e-05
    31: 8.18732601932209e-05
    16: 8.18732601932209e-05


## Entropia


```python
import math
#calcola l'entropia
entropy = 0
for letter, freq in rel_freq:
    entropy += -freq * math.log2(freq)

print("\nENTROPY -> {}\n".format(entropy))
```

    
    ENTROPY -> 4.430223307729059
    


# Frequenze standard dei caratteri ASCII in italiano


```python
with open("bibbia.txt", "rb") as f:
    bibbia = list(f.read())
```

## Frequenze assolute


```python
#conta le lettere
bibbia_freq = list(range(256))
for i in range(256):
    bibbia_freq[i] = [i, bibbia.count(i)]

#ordina le frequenze per valore e non per chiave
bibbia_freq.sort(key=lambda x: x[1])
bibbia_freq.reverse()
#print
print("--- FREQUENZE ASSOLUTE ---")
for i in range(256):
    if bibbia_freq[i][1] > 0:
        print("{} -> {}".format(repr(chr(bibbia_freq[i][0])), bibbia_freq[i][1]))
```

    --- FREQUENZE ASSOLUTE ---
    ' ' -> 2888
    'i' -> 2021
    'e' -> 1925
    'a' -> 1500
    'o' -> 1386
    't' -> 1148
    'n' -> 1127
    'l' -> 1032
    'r' -> 927
    's' -> 747
    'c' -> 711
    'd' -> 553
    'u' -> 374
    'm' -> 358
    'p' -> 328
    'b' -> 282
    'g' -> 212
    ',' -> 212
    'v' -> 183
    '.' -> 174
    '\n' -> 172
    'f' -> 144
    'z' -> 136
    'h' -> 126
    'Ã' -> 116
    '-' -> 101
    'I' -> 72
    "'" -> 71
    '*' -> 65
    ')' -> 65
    '(' -> 65
    'B' -> 64
    'C' -> 62
    'T' -> 59
    'A' -> 50
    '¨' -> 45
    'S' -> 39
    'D' -> 37
    'L' -> 34
    '1' -> 34
    'q' -> 32
    'P' -> 32
    'N' -> 32
    '9' -> 30
    ':' -> 28
    'G' -> 26
    'V' -> 25
    '\xa0' -> 24
    'E' -> 24
    '¹' -> 19
    '0' -> 18
    'M' -> 15
    '6' -> 15
    '7' -> 13
    '5' -> 13
    '²' -> 12
    'R' -> 12
    '/' -> 12
    '3' -> 11
    '=' -> 10
    'w' -> 9
    '8' -> 8
    '\x88' -> 7
    'k' -> 7
    'H' -> 7
    '4' -> 7
    '2' -> 7
    'Î' -> 6
    '¬' -> 6
    'U' -> 6
    'K' -> 6
    'F' -> 6
    ';' -> 6
    '©' -> 5
    'y' -> 5
    'X' -> 5
    'W' -> 4
    'O' -> 4
    'J' -> 4
    '"' -> 4
    'x' -> 3
    'Q' -> 3
    'Y' -> 2
    '×' -> 1
    '»' -> 1
    '±' -> 1
    '¯' -> 1
    '\x90' -> 1
    '\x89' -> 1
    'j' -> 1


## Correzioni


```python
for i in range(256):
    # r -> l -> t -> n
    if bibbia_freq[i][0] ==   ord('r'):
        bibbia_freq[i][0] = ord('l')
    elif bibbia_freq[i][0] == ord('l'):
        bibbia_freq[i][0] = ord('t')
    elif bibbia_freq[i][0] == ord('t'):
        bibbia_freq[i][0] = ord('n')
    elif bibbia_freq[i][0] == ord('n'):
        bibbia_freq[i][0] = ord('r')

    # v -> b -> g
    elif bibbia_freq[i][0] == ord('v'):
        bibbia_freq[i][0] = ord('b')
    elif bibbia_freq[i][0] == ord('b'):
        bibbia_freq[i][0] = ord('g')
    elif bibbia_freq[i][0] == ord('g'):
        bibbia_freq[i][0] = ord('v')

    # D -> I
    elif bibbia_freq[i][0] == ord('D'):
        bibbia_freq[i][0] = ord('I')
    elif bibbia_freq[i][0] == ord('I'):
        bibbia_freq[i][0] = ord('D')

    # p -> u
    elif bibbia_freq[i][0] == ord('p'):
        bibbia_freq[i][0] = ord('u')
    elif bibbia_freq[i][0] == ord('u'):
        bibbia_freq[i][0] = ord('p')

    # B -> P
    elif bibbia_freq[i][0] == ord('B'):
        bibbia_freq[i][0] = ord('P')
    elif bibbia_freq[i][0] == ord('P'):
        bibbia_freq[i][0] = ord('B')

    # . -> h -> f
    elif bibbia_freq[i][0] == ord('.'):
        bibbia_freq[i][0] = ord('h')
    elif bibbia_freq[i][0] == ord('h'):
        bibbia_freq[i][0] = ord('f')
    elif bibbia_freq[i][0] == ord('f'):
        bibbia_freq[i][0] = ord('.')

    # , -> z
    elif bibbia_freq[i][0] == ord(','):
        bibbia_freq[i][0] = ord('z')
    elif bibbia_freq[i][0] == ord('z'):
        bibbia_freq[i][0] = ord(',')

    # ( -> c
    elif bibbia_freq[i][0] == ord('('):
        bibbia_freq[i][0] = ord('C')
    elif bibbia_freq[i][0] == ord('C'):
        bibbia_freq[i][0] = ord('(')

    # G -> L -> W
    elif bibbia_freq[i][0] == ord('G'):
        bibbia_freq[i][0] = ord('L')
    elif bibbia_freq[i][0] == ord('L'):
        bibbia_freq[i][0] = ord('W')
    elif bibbia_freq[i][0] == ord('W'):
        bibbia_freq[i][0] = ord('G')

    # 8 -> N
    elif bibbia_freq[i][0] == ord('8'):
        bibbia_freq[i][0] = ord('N')
    elif bibbia_freq[i][0] == ord('N'):
        bibbia_freq[i][0] = ord('8')

    # 0 -> ?
    elif bibbia_freq[i][0] == ord('0'):
        bibbia_freq[i][0] = ord('?')
    elif bibbia_freq[i][0] == ord('?'):
        bibbia_freq[i][0] = ord('0')

    # A -> 1
    elif bibbia_freq[i][0] == ord('A'):
        bibbia_freq[i][0] = ord('1')
    elif bibbia_freq[i][0] == ord('1'):
        bibbia_freq[i][0] = ord('A')

    # 0xC3 -> \n
    elif bibbia_freq[i][0] == 0xC3:
        bibbia_freq[i][0] = ord('\n')
    elif bibbia_freq[i][0] == ord('\n'):
        bibbia_freq[i][0] = 0xC3 

    ## CONTINUARE CON ALTRE LETTERE...
```

# Creazione mappa


```python
mappa = dict()
for i in range(256):
    lettera_decifrata = bibbia_freq[i][0]
    lettera_cifrata = all_freq[i][0]
    mappa[lettera_decifrata] = lettera_cifrata
```


```python
import json
with open("nuova-mappa", "wt") as f:
    f.write(json.dumps(mappa))
```


```python
for k, v in mappa.items():
    print(f'{k}\t-> {v}')
```

    32	-> 125
    105	-> 225
    101	-> 90
    97	-> 0
    111	-> 248
    110	-> 60
    114	-> 44
    116	-> 171
    108	-> 202
    115	-> 68
    99	-> 47
    100	-> 255
    112	-> 114
    109	-> 111
    117	-> 189
    103	-> 204
    118	-> 50
    122	-> 108
    98	-> 226
    104	-> 29
    195	-> 169
    46	-> 52
    44	-> 41
    102	-> 30
    10	-> 187
    45	-> 166
    68	-> 64
    39	-> 48
    42	-> 53
    41	-> 254
    67	-> 78
    80	-> 77
    40	-> 35
    84	-> 5
    49	-> 37
    168	-> 209
    83	-> 107
    73	-> 252
    87	-> 219
    65	-> 96
    113	-> 195
    66	-> 32
    56	-> 203
    57	-> 227
    58	-> 197
    76	-> 75
    86	-> 122
    160	-> 74
    69	-> 214
    185	-> 146
    63	-> 249
    77	-> 133
    54	-> 25
    55	-> 11
    53	-> 215
    178	-> 180
    82	-> 158
    47	-> 87
    51	-> 241
    61	-> 136
    119	-> 130
    78	-> 222
    136	-> 150
    107	-> 46
    72	-> 245
    52	-> 34
    50	-> 240
    206	-> 185
    172	-> 165
    85	-> 164
    75	-> 137
    70	-> 126
    59	-> 105
    169	-> 191
    121	-> 172
    88	-> 103
    71	-> 135
    79	-> 51
    74	-> 31
    34	-> 16
    120	-> 253
    81	-> 251
    89	-> 250
    215	-> 247
    187	-> 246
    177	-> 244
    175	-> 243
    144	-> 242
    137	-> 239
    106	-> 238
    255	-> 237
    254	-> 236
    253	-> 235
    252	-> 234
    251	-> 233
    250	-> 232
    249	-> 231
    248	-> 230
    247	-> 229
    246	-> 228
    245	-> 224
    244	-> 223
    243	-> 221
    242	-> 220
    241	-> 218
    240	-> 217
    239	-> 216
    238	-> 213
    237	-> 212
    236	-> 211
    235	-> 210
    234	-> 208
    233	-> 207
    232	-> 206
    231	-> 205
    230	-> 201
    229	-> 200
    228	-> 199
    227	-> 198
    226	-> 196
    225	-> 194
    224	-> 193
    223	-> 192
    222	-> 190
    221	-> 188
    220	-> 186
    219	-> 184
    218	-> 183
    217	-> 182
    216	-> 181
    214	-> 179
    213	-> 178
    212	-> 177
    211	-> 176
    210	-> 175
    209	-> 174
    208	-> 173
    207	-> 170
    205	-> 168
    204	-> 167
    203	-> 163
    202	-> 162
    201	-> 161
    200	-> 160
    199	-> 159
    198	-> 157
    197	-> 156
    196	-> 155
    194	-> 154
    193	-> 153
    192	-> 152
    191	-> 151
    190	-> 149
    189	-> 148
    188	-> 147
    186	-> 145
    184	-> 144
    183	-> 143
    182	-> 142
    181	-> 141
    180	-> 140
    179	-> 139
    176	-> 138
    174	-> 134
    173	-> 132
    171	-> 131
    170	-> 129
    167	-> 128
    166	-> 127
    165	-> 124
    164	-> 123
    163	-> 121
    162	-> 120
    161	-> 119
    159	-> 118
    158	-> 117
    157	-> 116
    156	-> 115
    155	-> 113
    154	-> 112
    153	-> 110
    152	-> 109
    151	-> 106
    150	-> 104
    149	-> 102
    148	-> 101
    147	-> 100
    146	-> 99
    145	-> 98
    143	-> 97
    142	-> 95
    141	-> 94
    140	-> 93
    139	-> 92
    138	-> 91
    135	-> 89
    134	-> 88
    133	-> 86
    132	-> 85
    131	-> 84
    130	-> 83
    129	-> 82
    128	-> 81
    127	-> 80
    126	-> 79
    125	-> 76
    124	-> 73
    123	-> 72
    96	-> 71
    95	-> 70
    94	-> 69
    93	-> 67
    92	-> 66
    91	-> 65
    90	-> 63
    64	-> 62
    48	-> 61
    62	-> 59
    60	-> 58
    43	-> 57
    38	-> 56
    37	-> 55
    36	-> 54
    35	-> 49
    33	-> 45
    31	-> 43
    30	-> 42
    29	-> 40
    28	-> 39
    27	-> 38
    26	-> 36
    25	-> 33
    24	-> 28
    23	-> 27
    22	-> 26
    21	-> 24
    20	-> 23
    19	-> 22
    18	-> 21
    17	-> 20
    16	-> 19
    15	-> 18
    14	-> 17
    13	-> 15
    12	-> 14
    11	-> 13
    9	-> 12
    8	-> 10
    7	-> 9
    6	-> 8
    5	-> 7
    4	-> 6
    3	-> 4
    2	-> 3
    1	-> 2
    0	-> 1


# Testo finale
Alcuni caratteri utf-8 composti da 2 byte non è stato possibile decifrarli.
Credo dipenda sia dai caratteri di escape HTML che dalle frequenze trovate in `bibbia.txt`.

[https://www.wired.it/economia/business/2021/06/22/pmi-incentivi-transizione-energetica/](https://www.wired.it/economia/business/2021/06/22/pmi-incentivi-transizione-energetica/)
Perch,= la transizione energetica in Italia va cos,R a rilento?
Ci sono potenzialmente H- gigaWatt da fotovoltaico che potrebbero essere realizzati in 1/ mesi. La met,� delle installazioni sul solare ,S ferma per burocraziaT in un anno ,S stato approvato solo 1 impianto eolico. M se cambiassimo passo? :arebbe possibile autoprodurre il 3-� di energia in dieci anni
Mnergia perch,S la transizione in Italia va cos,R a rilento

Non ci ,S mai stato cos,R tanto bisogno di energia in Italia� sia per il presente che in prospettiva. M mai come adesso il settore imprenditoriale delle rinnovabili sarebbe prontoT secondo l'DA9lleanza per il fotovoltaico E che riunisce alcuni dei principali operatori italiani per l*energia solare E H- gigaWatt da progetti per impianti solari che potrebbero essere realizzati nei prossimi 1/ mesi sono ancora in attesa di autorizzazione. Progetti di investimento privato� senza oneri statali� che valgono 7� miliardi di euroT gi,� presentati e pronti per la messa a terra. 9l momento� tutti fermi. 'Dqwuesto ,S il Paese del sole Ve del solare�� ma occorre recuperare il tempo perduto'D8� dichiarava al Corriere della :era a maggio (-(1 il ministro della Oransizione ecologica VBite� yoberto Cingolani. Il 1/ gennaio (-(( si insedia la Commissione Nazionale PniecEPnrr� con l*obiettivo dichiarato dallo stesso ministro di 'Dqimprimere un'DAulteriore accelerazione nella realizzazione di impianti per l'DAenergia rinnovabile'D8. 

In un anno e mezzo� di )uesto sprint� non rimane traccia. La Commissione deve analizzare 7/- impianti di energia rinnovabile secondo i dati pubblicati dal Bite. �i )uesti� fino alla fine di settembre scorso solo 1 eolico era stato approvato� mentre il �(� dei 7(( impianti dedicati al solare sono ancora alla prima fase dell'DAiter burocratico� e solo il H(� ha raggiunto la seconda fase di approvazione. 

Perch,= ,= successo? yimpalli approvativi� tra Commissione e ministeri. M )uindi una )uestione culturale� dove al tema del nimbk Vnot in mk bac2kard� non nel mio cortile� si unisce l*ignoranza delle nuove soluzioni tecnologiche per la messa a terra di impianti solari. Lasciando scorrere l*unica risorsa che in una crisi energetica pronta ad esplodere nel (-(7 non ci possiamo pi,6 permettere di perdereT il tempo.
Mcco perch,= i progetti sono bloccati 

Innanzitutto� una considerazione lineare. 'Dq:e non produciamo energia verde per ridurre la dipendenza di gas e continueremo a ritardare il phaseEout delle centrali a carbone e di altre energie fossili� non riusciremo a raggiungere rapidamente la transizione energetica'D8� spiega 9ndrea Cristini� portavoce dell'DA9lleanza per il fotovoltaico. Non sar,� possibile sostituire il gas con l*idrogeno verde e avviare )uel processo di trasformazione dell*approvvigionamento energetico utile a centrare gli obiettivi degli 9ccordi di Parigi o )uelli della Commissione von der Leken per rendere l*Muropa il primo continente a impatto zero della storia dell*umanit,� entro il (-�-. 

Ba se non basta pi,6 la spinta del riscaldamento globale� ,S )uella della crisi energetica che dovrebbe accelerare il processoT se anche )uest*inverno ci saranno scorte sufficienti di gas� gi,� ad inizio del prossimo anno avremo sicuramente ancora pi,6 bisogno di energia. M possibile che la risposta a )uesta domanda in Italia arrivi in modo rilevante anche dalle rinnovabili? 'Dq9bbiamo decine di gigaWatt bloccati E segue Cristini E. :tefano �onnarumma Vad di :nam� ndr� ha detto che al momento ci sono (/- gigaWatt in domande di connessione'D8. Ba si tratta di una stima assoluta� mentre 

'Dq;na settantina di gigaWatt sono al momento bloccati in iter normativiT )uindi potrebbero essere immediatamente fruibili e velocemente realizzati. Idealmente� potremmo gi,� avere una decina di gigaWatt in produzione da rinnovabili gi,� dall'DAanno prossimo e a seguire dieci ogni anno. Ba l'DAapprovazione per )uelli del (-(7 deve arrivare gi,� adesso'D8.

Perch,= i progetti sono bloccati? Kli impianti fotovoltaici non sono approvati innanzitutto per motivi di interesse locale. Il classico tema del nimbkT bello il parco eolico e solare� ma non nel mio comune. La crisi energetica ha se non altro sbloccato )uesti pregiudizi. 'DqLa considerazione a livello locale ,S cambiata enormemente negli ultimi mesi e )uesto spero e credo che porter,� ad un cambiamento anche a livello di normative nazionali. Purtroppo abbiamo dovuto attendere che i prezzi dell'DAenergia arrivassero a livelli folli per accendere il campanello d'DAallarme e ricordarci che gran parte dell'DAenergia possiamo autoprodurla attraverso le fonti rinnovabili'D8� continua il portavoce. Il motivo 'Dqculturale'D8 ,S uno dei due fattori principali. 

C*,S )uindi una ragione tecnicaT i progetti di impianti rinnovabili non passano l*iter approvativo. 'Dq:ul mercato sono stati autorizzati impianti per )ualche gigaT 3E/ tra fotovoltaico ed eolico. Uvvero nulla. wuindi ad oggi il X3� dei progetti da fonte rinnovabile sono in forte ritardo in termini di autorizzazione o sono stati bocciati o non vanno bene. :tatisticamente ,S difficile pensare che le aziende e i tecnici che ci lavorano siano tutti cos,R scarsi da aver sbagliato X�3 progetti su 1-'D8� spiega Cristini. 
Puntare all*autoproduzione energetica

Passare alle rinnovabili conviene sia da un punto di vista economico che nella prospettiva dell*autoproduzione energetica. In primo luogo� il prezzoT secondo i dati dell*organizzazione di Cristini� il fotovoltaico� senza necessit,� di alcun incentivo� costa in media molto meno del gas Vcirca /- euro5B�h contro (�- euro5B�h�. :ono stime in linea con i numeri globali che mostrano come il costo delle varie fonti di energia rinnovabile continui a diminuire.

'Dq,� davvero incredibile che un Paese come l'DAItalia che ha vento e sole a volont,� non possa realizzare )uesti impianti in breve tempo E dice Cristini E. Ipotizziamo di poter produrre 1- gigaWatt da rinnovabili ogni anno� un risultato fattibile in termini produttiviT in 1- anni arriveremmo a 1-- gigaWatt. wuesti 1-- di solo fotovoltaico� sommati all*eolico� al geotermico e all*idroelettrico� possono concorrere a raggiungere il 3-� di autoproduzione energetica. :ignificano ridurre le importazioni e abbattere il consumo di gas'D8.

�ino al (-(1 la domanda energetica italiana ,S stata intorno ai 7(- teraWatt ora l*anno� "� in pi,6 rispetto al (-(- Vanno della pandemia� )uindi di consumi energetici non lineari rispetto all*ordinario�. Nei prossimi anni� il consumo energetico elettrico 'Dqaumenter,�T ad esempio� per la domanda di mobilita sostenibile o climatizzazione degli ambienti interni anche al fine di ridurre il gas. Insomma� se produrremo )ualche giga in pi,6� sar,� sicuramente utile'D8� continua Cristini.

'DqConsideriamo che 1-- giga di fotovoltaico producono circa 1�- teraWatt5ora di energia elettrica ogni anno. �a fonti rinnovabili in Italia abbiamo gi,� una produzione di circa 1-- teraWatt5ora l*annoT per )uesto� con altri 1-- giga da fotovoltaico si realizzano altri 1�- teraWatt5ora. :ommati all*energia elettrica prodotta dall*eolico� dall*eolico offshore� dal gas� raggiungeremmo una percentuale molto alta di autoproduzione4� prosegue l*esperto. Circa� appunto� il 3-� del totale necessario a soddisfare la domanda nazionale di energia elettrica.
�ove installare gli impianti

;na obiezione ricorrente all*installazione di impianti fotovoltaici ,S )uella del consumo di suolo. 9nche per )uesto ,S nata la soluzione dell*agrivoltaico� che consente l*installazione dei pannelli nei campi coltivati. 

L*obiezione )uindi si ,S spostata al fatto che i pannelli rubano terreno ai campi agricoli. Cristini riporta la posizione dei produttoriT 'Dqwuando sento direT 'DG9h� ma se mettiamo i pannelli a terra� nei campi� che cosa ci mangiamo?'DA� significa che non si conosce l*impatto di consumo di suolo dell*agrivoltaico'D8. :econdo i dati dell*9lleanza per il fotovoltaico� per produrre 1-- gigaWatt da )uesta fonte ci vogliono circa 1�-mila ettari di terrenoT non deve essere necessariamente tutto agricolo� ma se anche lo fosse 'Dqandremmo a perdere una minima parte di terreno in un Paese che solo in Puglia ha 1 milione di ettari utilizzabili. Come pu,F )ualche migliaio di ettaro in meno impattare tanto sulla produzione agricola? Credo che incida molto di pi,6 il rincaro energetico� che probabilmente uccide molte delle aziende agricole oggi in Italia. 9ttenzioneT l*agrivoltaico non viene installato dove ci sono linee di valore Vcome i vitigni della �ranciacorta o le uve del Primitivo in Puglia� e non danneggia )uindi la produzione agricola di filiere per cos,R dire delicate. Ba dove non ci sono )uesti vincoli� l'DAagrivoltaico ci consente di creare meccanismi vincenti sia per l*energia che per l*agricoltura4. Chiaramente� gli impianti del fotovoltaico andrebbero prima installati in altri luoghiT le aree industriali dismesse� che anche il governo ha gi,� indicato come aree idonee. 'DqL,R sarebbe immediatamente fattibile un'DAautorizzazione rapida degli impianti'D8� conclude il portavoce.
Proposte per superare l*impasse burocratica

Le ambizioni dell*9lleanza� ovvero realizzare un potenziale produttivo realistico di circa 1-- gigaWatt nei prossimi dieci anni� superano gli obiettivi del Piano nazionale integrato per l*energia e il clima (-7- VPniec� che prevede la realizzazione di �- gigaWatt entro i prossimi otto anni. Ba sono dati dei produttori� )uindi proporzionati alla forza impiantistica che si pu,F davvero schierare sul territorio nazionale. 

Potremmo vivere di eolico?
Il caso J1 �ind� startup catalana che mette le turbine in alto mare� ci mostra vantaggi e difficolt,� di )uesto sistema energetico

:i tratta di ambizioni da perseguireT pensiamo alla Kermania� benchmar2 europeo per il fotovoltaico� che recentemente ha alzato il proprio obiettivo di produzione da rinnovabili a H-- gigaWatt entro il (-7�� soprattutto per sostituire gli impianti nucleari che andranno a dismettere. In Kermania c*,S per,F grande certezza sull'DAiter normativo e una dinamicit,� pi,6 elevata di )uella italiana sulle approvazioni. ,� )ui il punto iniziale dell*impasse che blocca la produzione nazionale di fotovoltaico. 9 tal proposito� Cristini propone due soluzioni per sbloccare gli iter approvativi. 4�a un punto di vista normativo� ci sono alcune vacanze che se fossero chiarite Vanche come dubbi interpretativi� sbloccherebbero molti progetti. wuindi� soffriamo una grande carenza di risorse umane. La Commissione Pniec5Pnrr e il Bite hanno pochissime unit,� che istruiscono le pratiche. Non ho dati ufficiali� ma si tratta di tre o )uattro i funzionari che devono istruire centinaia di pratiche in pochi mesiT l,R c'DA,S un collo di bottiglia.Probabilmente una soluzione immediata dovrebbe essere l*istituzione di una tas2 forceT assegnare nuove risorse umane agli uffici e accelerare la progressione di )uesti impianti che sono autorizzati solo se hanno il parere positivo della Commissione Pniec5Pnrr4. 

;n altro blocco dell*iter arriva dopo il parere della Commissione� attraverso la controverifica del Bit VBinistero delle infrastrutture e della mobilit,� sostenibili� che 'Dqdi fatto sta bloccando i progetti perch,= impiega molto tempo e spesso la risposta ,S negativa� rimandando )uindi )uesti progetti alla presidenza del Consiglio dei Binistri. wuindi il parere della Commissione� dove interviene comun)ue un altro parere del Binistero� non dovrebbe poi essere seguito da un nuovo parere'D8. U se )uesto fosse assolutamente necessario� basterebbe avere pi,6 personale competente impegnato nelle approvazioni. 

L*approvvigionamento energetico ,S una priorit,� nazionaleT l*autoproduzione ,S la meta condivisa. Per la prima volta� anche l*Italia potrebbe raggiungerla in tempi brevi. ,� il momento di accelerare davvero la transizione energeticaT se )uesto significa aumentare il personale competente nelle deliberazioni sugli impianti� ,S il momento di farlo )uanto prima possibile. Le risorse del Pnrr potrebbero essere utiliT creare lavoro pubblico per spingere la crescita del settore privato dedicato alle rinnovabili per abbattere le emissioni di CU( e i costi energetici. Mcco una vera catena del valore.
