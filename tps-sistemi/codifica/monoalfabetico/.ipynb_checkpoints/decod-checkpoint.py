import math
import string
import json

CHARSET = 'latin_1'

f = open("energy.cod", "rb")
content=list(f.read())
f.close()

f = open("bibbia.txt", "rb")
bibbia = list(f.read())
f.close()

# --- TROVA LE FREQUENZE ---
#conta le lettere
all_freq = list(range(256))
for i in range(256):
    all_freq[i] = [i, content.count(i)]

#ordina le frequenze per valore e non per chiave
all_freq.sort(key=lambda x: x[1])
all_freq.reverse()
#print
print("--- FREQUENZE ASSOLUTE ---")
for i in range(256):
    print("{} -> {}".format(repr(chr(all_freq[i][0])), all_freq[i][1]))
print()

# #calcola le frequenze relative
# rel_freq = list()
# for letter, freq in all_freq:
#     rel_freq.append((letter, freq/len(content)))

# #print
# print("--- FREQUENZE RELATIVE ---")
# for i, j in rel_freq:
#     print("{}: {}".format(repr(i), j))
# print()

# #calcola l'entropia
# entropy = 0
# for letter, freq in rel_freq:
#     entropy += -freq * math.log2(freq)

# print("\nENTROPY -> {}\n".format(entropy))


# --- TROVA LE FREQUENZE ---
#conta le lettere
bibbia_freq = list(range(256))
for i in range(256):
    bibbia_freq[i] = [i, bibbia.count(i)]

#ordina le frequenze per valore e non per chiave
bibbia_freq.sort(key=lambda x: x[1])
bibbia_freq.reverse()
#print
print("--- FREQUENZE ASSOLUTE ---")
for i in range(256):
    print("{} -> {}".format(repr(chr(bibbia_freq[i][0])), bibbia_freq[i][1]))
print()

## ------- QUI HO DUE LISTE DI LISTE DI 256 ELEMENTI -------

##Correzioni sulla frequenza
for i in range(256):
    # r -> l -> t -> n
    if bibbia_freq[i][0] ==   ord('r'):
        bibbia_freq[i][0] = ord('l')
    elif bibbia_freq[i][0] == ord('l'):
        bibbia_freq[i][0] = ord('t')
    elif bibbia_freq[i][0] == ord('t'):
        bibbia_freq[i][0] = ord('n')
    elif bibbia_freq[i][0] == ord('n'):
        bibbia_freq[i][0] = ord('r')

    # v -> b -> g
    elif bibbia_freq[i][0] == ord('v'):
        bibbia_freq[i][0] = ord('b')
    elif bibbia_freq[i][0] == ord('b'):
        bibbia_freq[i][0] = ord('g')
    elif bibbia_freq[i][0] == ord('g'):
        bibbia_freq[i][0] = ord('v')

    # D -> I
    elif bibbia_freq[i][0] == ord('D'):
        bibbia_freq[i][0] = ord('I')
    elif bibbia_freq[i][0] == ord('I'):
        bibbia_freq[i][0] = ord('D')

    # p -> u
    elif bibbia_freq[i][0] == ord('p'):
        bibbia_freq[i][0] = ord('u')
    elif bibbia_freq[i][0] == ord('u'):
        bibbia_freq[i][0] = ord('p')

    # B -> P
    elif bibbia_freq[i][0] == ord('B'):
        bibbia_freq[i][0] = ord('P')
    elif bibbia_freq[i][0] == ord('P'):
        bibbia_freq[i][0] = ord('B')

    # . -> h -> f
    elif bibbia_freq[i][0] == ord('.'):
        bibbia_freq[i][0] = ord('h')
    elif bibbia_freq[i][0] == ord('h'):
        bibbia_freq[i][0] = ord('f')
    elif bibbia_freq[i][0] == ord('f'):
        bibbia_freq[i][0] = ord('.')

    # , -> z
    elif bibbia_freq[i][0] == ord(','):
        bibbia_freq[i][0] = ord('z')
    elif bibbia_freq[i][0] == ord('z'):
        bibbia_freq[i][0] = ord(',')

    # ( -> c
    elif bibbia_freq[i][0] == ord('('):
        bibbia_freq[i][0] = ord('C')
    elif bibbia_freq[i][0] == ord('C'):
        bibbia_freq[i][0] = ord('(')

    # G -> L -> W
    elif bibbia_freq[i][0] == ord('G'):
        bibbia_freq[i][0] = ord('L')
    elif bibbia_freq[i][0] == ord('L'):
        bibbia_freq[i][0] = ord('W')
    elif bibbia_freq[i][0] == ord('W'):
        bibbia_freq[i][0] = ord('G')

    # 8 -> N
    elif bibbia_freq[i][0] == ord('8'):
        bibbia_freq[i][0] = ord('N')
    elif bibbia_freq[i][0] == ord('N'):
        bibbia_freq[i][0] = ord('8')

    # 0 -> ?
    elif bibbia_freq[i][0] == ord('0'):
        bibbia_freq[i][0] = ord('?')
    elif bibbia_freq[i][0] == ord('?'):
        bibbia_freq[i][0] = ord('0')

    # A -> 1
    elif bibbia_freq[i][0] == ord('A'):
        bibbia_freq[i][0] = ord('1')
    elif bibbia_freq[i][0] == ord('1'):
        bibbia_freq[i][0] = ord('A')

    # 0xC3 -> \n
    elif bibbia_freq[i][0] == 0xC3:
        bibbia_freq[i][0] = ord('\n')
    elif bibbia_freq[i][0] == ord('\n'):
        bibbia_freq[i][0] = 0xC3 

    ## CONTINUARE CON ALTRE LETTERE...

mappa = dict()
for i in range(256):
    lettera_decifrata = bibbia_freq[i][0]
    lettera_cifrata = all_freq[i][0]
    mappa[lettera_decifrata] = lettera_cifrata

f = open("nuova-mappa",'wt')
f.write( json.dumps( mappa ) )
f.close()
