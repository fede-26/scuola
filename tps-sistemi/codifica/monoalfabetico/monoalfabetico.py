#!/usr/bin/python3

import random, json

CHARSET = 'latin_1'

def creaMappa(fn):
	''' crea una mappa a 8 bit e salvala in fn con un formato del tipo ord(char)\t ord(char)\n '''
	tutti = [x for x in range(256) ]
	random.shuffle(tutti)
	mappa = dict()
	for i in range(256): mappa[i] = tutti[i]

	f = open(fn,'wt')
	f.write( json.dumps( mappa ) )
	f.close()

def leggiMappa(fn):
	''' leggi dal file una mappa nel formato ord(char)\tord(char)\n '''
	f = open(fn)
	s = f.read()
	f.close()
	mappa = json.loads( s )
	keys = [x for x in mappa.keys() ]
	for k in keys:	mappa[ int(k) ] = mappa[k]
	return mappa

def leggiMappaPerDecifrare(fn):
	''' come leggi mappa, ma scambia chiave con valore'''
	mappa = leggiMappa(fn)
	revmap = dict()
	for k,v in mappa.items():	revmap[v] = k
	return revmap

def cifra(text,cyphertext,mapfile):
	''' leggi il file text e cifralo nel cyphertext dato il mpafile'''
	mappa = leggiMappa(mapfile)
	f = open(text,'rb')
	s = f.read()
	f.close()
	
	cifrato = ''
	for c in s:	cifrato += chr( mappa[c] )

	f = open( cyphertext,'wb')
	f.write(cifrato.encode(CHARSET) )
	f.close()

def decifra(cyphertext,decodetext,mapfile):
	''' leggi il file cyphertext e decifralo nel decodetext dato il mpafile'''
	mappa = leggiMappaPerDecifrare(mapfile)
	f = open(cyphertext,'rb')
	s = f.read()
	f.close()

	decifrato = ''
	for c in s:	decifrato += chr( mappa[int(c)] )
	
	f = open( decodetext,'wb')
	f.write(decifrato.encode(CHARSET) )
	f.close()

if __name__=='__main__':
	import sys
	"""
	parametri:	--creamappa	mapfile
				--codifica	mapfile 	textfile	cyphertextfile
				--decodifica	mapfile 	cyphertextfile	decodedfile
	print(len(sys.argv) )
	for x in sys.argv: print(x)"""
	if len(sys.argv)>2:
		if sys.argv[1] == '--creamappa':
			creaMappa(sys.argv[2])

		elif sys.argv[1] == '--codifica' and len(sys.argv)>4:
			mapfile, textfile, cyphertext = sys.argv[2], sys.argv[3], sys.argv[4]
			cifra(textfile,cyphertext,mapfile)
		
		elif sys.argv[1] == '--decodifica' and len(sys.argv)>4:
			mapfile, cyphertext, decodetext = sys.argv[2], sys.argv[3], sys.argv[4]
			decifra(cyphertext,decodetext, mapfile)