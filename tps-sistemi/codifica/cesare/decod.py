#!/usr/bin/env python3

## AUTHOR: Federico Zotti

import string
import math

# --- LEGGI IL FILE ---
file = open("pcomc.cod")
content = file.read()
upper_content = '' + content
file.close()
content = content.lower()

# --- TROVA LE FREQUENZE ---
#conta le lettere
all_freq = {}
for i in content:
    if i in all_freq:
        all_freq[i] += 1
    else:
        all_freq[i] = 1

#ordina le frequenze per valore e non per chiave
all_freq = sorted(all_freq.items(), key=lambda x:x[1])

#print
print("--- FREQUENZE ASSOLUTE ---")
for i, j in all_freq:
    print("{}: {}".format(repr(i), j))
print()

#calcola le frequenze relative
rel_freq = list()
for letter, freq in all_freq:
    rel_freq.append((letter, freq/len(content)))

#print
print("--- FREQUENZE RELATIVE ---")
for i, j in rel_freq:
    print("{}: {}".format(repr(i), j))
print()

#calcola l'entropia
entropy = 0
for letter, freq in rel_freq:
    entropy += -freq * math.log2(freq)

print("\nENTROPY -> {}\n".format(entropy))

# --- TROVA LO SHIFT ---
ALPHABET = string.ascii_lowercase

#rimuovi tutte le lettere accentate e la punteggiatura
filtered_freq = list()
for i, j in all_freq:
    if i in ALPHABET:
        filtered_freq.append((i, j))

print("--- FREQUENZE ASSOLUTE FILTRATE ---")
print(filtered_freq)
print()

#calcola lo shift come la differenza tra la lettera più comune dell'alfabeto (e) e la lettera più comune nel file
shift =  ALPHABET.index(filtered_freq[-1][0]) - ALPHABET.index('e')

print("\nSHIFT: {}".format(shift))
print("{} -> e\n".format(filtered_freq[-1][0]))

# --- DECODIFICA ---
decoded = ""

for i in content:
    #se la lettera fa parte dell'alfabeto allora trova la corrispettiva con lo shift calcolato
    #se la lettera non fa parte dell'alfabeto allora non cambiarla
    if i in ALPHABET:
        new_i = ALPHABET[(ALPHABET.index(i) - shift)]
        decoded += new_i
    else:
        decoded += i

# --- RIFORMATTA LE MAIUSCOLE ---
UPPER_ALPHABET =  string.ascii_uppercase

decoded_upper = ""

for i, letter in enumerate(decoded):
    if upper_content[i] in UPPER_ALPHABET:
        decoded_upper += letter.upper()
    else:
        decoded_upper += letter

print("--- TESTO DECIFRATO ---")
print(decoded_upper)

## https://en.wikipedia.org/wiki/Caesar_cipher