#!/usr/bin/fish

set SOCKETS (ss -tnpH)
for SOCKET in $SOCKETS
    set SOURCE_IP (echo $SOCKET | awk '{ print $5 }')
    set PIDS_UNFORMATTED (echo $SOCKET | awk '{print $5}' | awk -F "," '/pid=[0-9]*/{ print $1 }')
    echo $PIDS_UNFORMATTED
    # echo $SOCKET | awk '{ print $6 }'
end
