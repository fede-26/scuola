//! # Libreria per il server Jn
//!
//! Questa è la libreria con funzione usate dal server Jn

use serde::{Deserialize, Serialize};
use std::{
    io::{Read, Write},
    net::{Shutdown, TcpStream},
};

use rsa_utils::{generate_group, phi, inverse};

/// Lista di richieste che il client può chiedere al server.
enum RequestList {
    /// Calcola il phi di un numero.
    PHI,

    /// Calcola gli elementi nel gruppo moltiplicativo modulo n.
    NUMELEM,

    ///Calcola la cardinalità del gruppo moltiplicativo modulo n.
    CARDINALITY,

    /// Elenca gli elementi nel gruppo moltiplicativo modulo n.
    ELEMENTS,

    /// Calcola l'inverso di x nel gruppo moltiplicativo modulo n.
    INVERSE,
}

/// Struct della richiesta inviata dal client.
#[derive(Serialize, Deserialize)]
struct Request {
    /// Numero utilizzato per creare il gruppo moltiplicativo modulo n.
    n: i64,
    /// Richiesta del client.
    /// Per trasformare la stringa in enum, utilizzare la funzione `string_to_request`.
    richiesta: String,
    /// Numero di cui calcolare l'inverso.
    /// È un `Option<i64>` perchè solo nella richiesta `RequestList::REVERSE` è necessario.
    x: Option<i64>,
}

/// Struct della risposta quando `response` è un numero.
/// 
/// Se `response` è un vettore, utilizzare `ResponseList`.
#[derive(Serialize, Deserialize)]
struct ResponseN {
    /// È l'intero calcolato, se è 0 allora è presente un errore
    response: i64,
    error: String,
}

/// Struct della risposta quando `response` è un vettore.
/// 
/// Se `response` è un intero, utilizzare `ResponseN`.
#[derive(Serialize, Deserialize)]
struct ResponseList {
    /// È il vettore calcolato, se è \[0\] allora è presente un errore
    response: Vec<i64>,
    error: String,
}

/// Funzione che converte il buffer di dati della socket in json.
/// 
/// Se la conversione va a buon fine, ritorna un `Ok(Request)`.
/// 
/// # Esempi
/// 
/// ```ignore
/// let data = b"{\"n\": 10, \"richiesta\": \"phi\"}";
/// let request = parse_json(data);
/// assert_eq!(request.unwrap().n, 10);
/// assert_eq!(request.unwrap().richiesta, "phi");
/// assert_eq!(request.unwrap().x, None);
/// ```
/// 
/// # Errori
/// 
/// Se la conversione non va a buon fine, ritorna un `Err(String)`.
fn parse_json(data: &[u8]) -> Result<Request, String> {
    let request: Request = match serde_json::from_slice(data) {
        Ok(it) => it,
        Err(err) => return Err(format!("Error parsing JSON: {}", err)),
    };
    Ok(request)
}

/// Funzione che converte una stringa nell'enum con le richieste (`RequestList`).
/// 
/// Se la conversione va a buon fine, ritorna un `Ok(RequestList)`.
/// 
/// # Esempi
/// 
/// ```ignore
/// let request = Request {
///   n: 10,
///   richiesta: "phi".to_string(),
///   x: None,
/// };
/// let req_type = string_to_request(&request);
/// assert_eq!(req_type.unwrap(), RequestList::PHI);
/// ```
/// 
/// # Errori
/// 
/// Se la conversione non va a buon fine, ritorna un `Err(String)`.
fn string_to_request(request: &Request) -> Result<RequestList, String> {
    let s = request.richiesta.as_str();

    match s {
        "phi" => Ok(RequestList::PHI),
        "numero di elementi nel gruppo" => Ok(RequestList::NUMELEM),
        "cardinalità del gruppo" => Ok(RequestList::CARDINALITY),
        "elenco degli elementi" => Ok(RequestList::ELEMENTS),
        "richiesta dell'inverso" => Ok(RequestList::INVERSE),
        // Se la richiesta non è tra quelle elencate, ritorna un errore
        _ => Err("Error: request not found".to_string()),
    }
}

/// Funzione che gestisce la richiesta del client.
/// 
/// Riceve in input il buffer dalla socket e ritorna la risposta da inviare al client.
/// 
/// # Struttura
/// 
/// - Parsing della richiesta;
/// - Esecuzione della funzione richiesta;
/// - Creazione della risposta;
/// - Ritorno della risposta.
/// 
/// # Esempi
/// 
/// ```ignore
/// let data = b"{\"n\": 10, \"richiesta\": \"phi\"}";
/// let response = handle_request(data);
/// assert_eq!(response, "{\"response\":4,\"error\":\"\"}");
/// ```
/// 
/// # Errori
/// 
/// Se viene generato qualche errore all'interno esso viene inserito nella risposta.
fn handle_request(data: &[u8]) -> String {
    // Parsing del json
    let request: Request;
    match parse_json(data) {
        Ok(req) => request = req,
        Err(err) => {
            let response = ResponseN {
                response: 0,
                error: format!("{}", err),
            };
            let response = serde_json::to_string(&response).unwrap();
            return response;
        }
    };

    // Parsing del tipo della richiesta
    let req_type: RequestList;
    match string_to_request(&request) {
        Ok(req) => req_type = req,
        Err(err) => {
            let response = ResponseN {
                response: 0,
                error: format!("{}", err),
            };
            let response = serde_json::to_string(&response).unwrap();
            return response;
        }
    }

    // Esecuzione della richiesta e creazione della risposta
    match req_type {
        RequestList::PHI | RequestList::CARDINALITY | RequestList::NUMELEM => {
            let result = phi(request.n);
            match result {
                Ok(res) => {
                    let response = ResponseN {
                        response: res,
                        error: "".to_string(),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
                Err(err) => {
                    let response = ResponseN {
                        response: 0,
                        error: format!("{}", err),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
            }
        }
        RequestList::ELEMENTS => {
            let result = generate_group(request.n);
            match result {
                Ok(res) => {
                    let response = ResponseList {
                        response: res,
                        error: "".to_string(),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
                Err(err) => {
                    let response = ResponseList {
                        response: vec![0],
                        error: format!("{}", err),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
            }
        }
        RequestList::INVERSE => {
            let x: i64;
            match request.x {
                Some(it) => x = it,
                None => {
                    let response = ResponseN {
                        response: 0,
                        error: "Error: x not found".to_string(),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
            }
            let result = inverse(request.n, x);
            match result {
                Ok(res) => {
                    let response = ResponseN {
                        response: res,
                        error: "".to_string(),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
                Err(err) => {
                    let response = ResponseN {
                        response: 0,
                        error: format!("{}", err),
                    };
                    let response = serde_json::to_string(&response).unwrap();
                    return response;
                }
            }
        }
    }
}

/// Funzione che gestisce la comunicazione con il client dopo la connessione.
/// 
/// # Struttura
/// 
/// - Invio immediato del messaggio di benvenuto;
/// - Ricezione della richiesta;
/// - Gestione della richiesta;
/// - Invio della risposta;
/// - Chiusura della connessione.
pub fn handle_client(mut stream: TcpStream) {
    // Crea un buffer di 1024 byte
    let mut data = [0 as u8; 1024];

    // Invia il messaggio di benvenuto
    stream
        .write("Server Jn di Federico Zotti V1.0 del 2023-01-15".as_bytes())
        .unwrap();

    // Legge i dati dalla socket
    match stream.read(&mut data) {
        Ok(size) => {
            // Gestisce la richiesta
            let response = handle_request(&data[0..size]);
            stream.write(response.as_bytes()).unwrap();

            // Chiude la connessione
            stream.shutdown(Shutdown::Both).unwrap();
        }
        Err(_) => {
            println!(
                "An error occurred, terminating connection with {}",
                stream.peer_addr().unwrap()
            );
            // Chiude la connessione
            stream.shutdown(Shutdown::Both).unwrap();
        }
    }
    {}
}
