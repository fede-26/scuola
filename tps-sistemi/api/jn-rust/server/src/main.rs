//! Server Jn di Federico Zotti V1.0 del 2023-01-15
//! 
//! Questo server permette di effettuare calcoli sul gruppo moltiplicativo modulo n.
//! 
//! Calcoli disponibili:
//! - phi(n);
//! - cardinalità di un gruppo;
//! - numero di elementi in un gruppo;
//! - lista degli elementi in un gruppo;
//! - calcolo inverso di un numero in un gruppo.
//! 
//! Per effettuare un calcolo, inviare una richiesta in formato JSON con la seguente struttura:
//! 
//! ```
//! {
//!   "request": "phi",
//!   "n": 10,
//!   "x": Optional int
//! }
//! ```
//! 
//! La risposta sarà una stringa JSON con la seguente struttura:
//! 
//! ```
//! {
//!   "response": 4,
//!   "error": ""
//! }
//! ```
//! 
//! In caso di errore, la risposta sarà una stringa JSON con la seguente struttura:
//! 
//! ```
//! {
//! "response": 0,
//! "error": "Errore"
//! }
//! ```


use clap::Parser;
use std::net::TcpListener;
use std::thread;

// Importa la funzione dalla libreria nel crate
use jn_rust_server::handle_client;

// Parsing degli argomenti passati al programma (--help per vederli)
/// Jn server
#[derive(Parser, Debug)]
#[command(name = "jn-rust-server")]
#[command(author = "Federico Zotti")]
#[command(version = "1.0")]
#[command(about = "Server per effettuare calcoli sul gruppo moltiplicativo modulo n.")]
#[command(long_about = None)]
struct Args {
    /// Use the tcp protocol instead of udp
    #[arg(short, long, default_value_t = ("0.0.0.0".to_string()))]
    ip: String,

    /// View the raw message incoming
    #[arg(short, long, default_value_t = 65020)]
    port: u16,
}

/// Funzione main.
/// Avvia la socket e aspetta le connessioni.
/// Per ogni connessione, crea un thread e chiama la funzione [`handle_client`](jn_rust_server::handle_client).
/// 
/// # Struttura
/// 
/// - Parsing degli argomenti;
/// - Bind della socket;
/// - Ciclo infinito per aspettare le connessioni;
/// - Accetta le connessioni e crea un nuovo thread.
fn main() {
    let args = Args::parse();
    let listener = TcpListener::bind(format!("{}:{}", args.ip, args.port)).unwrap();
    // Accetta le connessioni e crea un nuovo thread per ogni connessione
    println!("Server listening on {}:{}", args.ip, args.port);
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr().unwrap());
                thread::spawn(move || {
                    // connection succeeded
                    handle_client(stream)
                });
            }
            Err(e) => {
                println!("Error: {}", e);
                /* connection failed */
            }
        }
    }
    // Chiude la socket
    drop(listener);
}
