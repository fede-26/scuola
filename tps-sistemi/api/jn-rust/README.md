---
title: "Server per il calcolo del gruppo moltiplicativo modulo n ($J_n$)"
author:
  - Federico Zotti
date: 15/01/2023
# Settings
titlepage: true
toc: true
#toc-own-page: true
lang: it
standalone: true
#highlight-style: tango  #use --highlight-style tango
pdf-engine: xelatex
katex: true
geometry:
  - a4paper
  - margin=3.5cm
documentclass: scrartcl
pagestyle: headings
colorlinks: true
links-as-notes: true
---

\pagebreak

# Descrizione

Questo progetto è composto da un server principale che si occupa di eseguire calcoli su un gruppo moltiplicativo modulo $n$ ($J_n$) e da un semplice client per generare le richieste.
Il server e il client comunicano tramite una socket TCP con uno scambio di dati in formato JSON.
Il server inoltre crea un nuovo thread per ogni connessione con un client, ciò vuol dire che è possibile connettersi con più client contemporaneamente.

# Comunicazione

Creato con [sequencediagram.org](https://sequencediagram.org/).

![](https://fakeimg.pl/300/?text=Seq diag)
<!-- here svg of seq diag -->

## Benvenuto

Appena il client si connette al server riceve subito la seguente stringa:
```
"Server Jn di Federico Zotti V_x.y_ del _yyyy-mm-dd_".
```
Essa serve per riconoscere il processo con cui stiamo comunicando se, su una macchina, ne sono presenti altri.

La stringa è _hardcoded_ nel programma e deve essere modificata manualmente quando si effettuano cambiamenti al programma.

## Richiesta

La richiesta è formattata:

```json
{
    "n": 123,
    "richiesta": "elenco degli elementi",
    "x": 4
}
```

- `n`: è il modulo del gruppo moltiplicativo da generare;
- `richiesta`: è il tipo di richiesta che vogliamo fare al server; può avere i seguenti valori:
  * `"phi"`: calcola $\varphi(n)$; <!-- aggiungere link a wikipedia per phi !!! -->
  * `"numero di elementi nel gruppo"`: restituisce il numero degli elementi in $J_n$ (uguale a $\varphi(n)$);
  * `"cardinalità del gruppo"`: restituisce la cardinalità del gruppo (uguale a $\varphi(n)$);
  * `"elenco degli elementi"`: restituisce la lista degli elementi del gruppo;
  * `"richiesta dell'inverso"`: restituisce l'inverso di un numero $x$ nel gruppo $\mod n$;
- `x`: necessaria solo nel caso della richiesta dell'inverso.


## Risposta

La risposta del server è formattata nel seguente modo:

```json
{
    "response": 123,
    "error": "Stringa di errore"
}
```

- `response`: il valore calcolato:
  * `int`: nel caso il valore sia solo un numero; per segnalare un errore il valore è `0`;
  * `vec`/`list`: nel caso siano un elenco di valori; per segnalare un errore il valore è `[0]`;
- `error`: contiene l'errore, nel caso ci sia.

\pagebreak

# Implementazione server in Rust

Tutta la logica del server è implementata in [Rust](https://www.rust-lang.org/) per la sua elevata efficienza rispetto a Python.

Il programma completo è diviso in 3 file principali:

- `rsa_utils`: è una collezione di funzioni matematiche utili e usate frequentemente;
- `jn_rust_server`:
  * `main.rs`: la funzione main del programma;
  * `lib.rs`: funzioni utilizzate dal programma;

## RSA Utils

Questa libreria non dipende dal server Jn ma contiene alcune funzioni ritenute più utili per sviluppare applicativi legati all'RSA e allo studio di gruppi.
Può essere importata in altri _crates_ per utilizzarne i metodi.

Contiene principalmente le seguenti funzioni:

### gcd

Funzione che calcola il massimo comun divisore tra due numeri.
Utilizza l'algoritmo di Euclide con ricorsività.

Prototipo:

```rust
pub fn gcd(a: i64, b: i64) -> Result<i64, String>
```

Esempio:

```rust
let a = 10;
let b = 5;
let x = rsa_utils::gcd(a, b);
assert_eq!(x, Ok(5));
```

Se `a <= 0` o `b < 0`, viene restituito un errore.

### egcd

Funzione che calcola il massimo comun divisore tra due numeri e i coefficienti dell'[identità di Bézout](https://it.wikipedia.org/wiki/Identit%C3%A0_di_B%C3%A9zout).
Utilizza l'[algoritmo di Euclide esteso](https://it.wikipedia.org/wiki/Algoritmo_esteso_di_Euclide) senza ricorsività.

Restituisce una tupla con il massimo comun divisore e i numeri per l’identità di Bézout.

Prototipo:

```rust
pub fn egcd(a: i64, b: i64) -> Result<(i64, i64, i64), String>
```

Esempio:

```rust
let a = 10;
let b = 5;
let (gcd, x, y) = rsa_utils::egcd(a, b).unwrap();
assert_eq!(gcd, 5);
assert_eq!(x, 0);
assert_eq!(y, 1);
```

Se `a < 0` o `b < 0`, viene restituito un errore.

### generate_group

Funzione che ritorna un vettore contenente gli elementi del gruppo $J_n$.
Utilizza `gcd`.

Prototipo:

```rust
pub fn generate_group(n: i64) -> Result<Vec<i64>, String>
```

Esempio:

```rust
let n = 10;
let group = rsa_utils::generate_group(n);
assert_eq!(group, Ok(vec![1, 3, 7, 9]));
```

Se `n` è negativo, viene restituito un errore.

### phi

Funzione per calcolare $\varphi(n)$, anche conosciuta come [funzione di Eulero](https://it.m.wikipedia.org/wiki/Funzione_%CF%86_di_Eulero).
Non è ottimizzata è utilizza `gcd`.

Prototipo:

```rust
pub fn phi(n: i64) -> Result<i64, String>
```

Esempio:

```rust
let n = 10;
let x = rsa_utils::phi(n);
assert_eq!(x, Ok(4));
```

Se il numero è negativo, viene restituito un errore.

### inverse

Funzione che calcola il numero inverso in un gruppo $J_n$.

Prototipo:

```rust
pub fn inverse(n: i64, a: i64) -> Result<i64, String>
```

Esempio:

```rust
let n = 11;
let a = 3;
let x = rsa_utils::inverse(n, a);
assert_eq!(x, Ok(4));
```

\pagebreak

## Server main

Contiene la struttura principale del server.
Si occupa di fare il parsing degli argomenti passati da terminale e avviare la socket.
Per ogni connessione crea un thread con la funzione `handle_request` presente in `lib.rs`.

Per il parsing degli argomenti utilizza [clap](https://crates.io/crates/clap).
Essi vengono definiti in una struct, che poi viene istanziata con all'interno i valori.
Tramite `--help` è possibile vedere un messaggio con le informazioni di aiuto del programma.
Gli unici parametri sono:

- `ip`: indica l'interfaccia su cui la socket deve fare la _bind_ (default: 0.0.0.0);
- `port`: indica la porta su cui stare in ascolto (default: 65020).

La struttura è:

- parsing degli argomenti;
- bind della socket;
- loop infinito per aspettare le connessioni;
- accetta ogni connessione e crea nuovi thread.

## Server lib

\pagebreak

# Implementazione client in Python
