import socket
import argparse
import json
from enum import Enum

# porta e interfaccia di default
INTF = '0.0.0.0'
PORT = '2137'
DESCRIPTION = 'Server Jn di Federico Zotti V1.0 del 2023-01-12'

# Richieste possibili
class Req_list(Enum):
    PHI = 'phi'
    NUMELEM = 'numero di elementi nel gruppo'
    CARDINALITY = 'cardinalità del gruppo'
    ELEMENTS = 'elenco degli elementi'
    REVERSE = 'richiesta dell\'inverso'

# ==PARSE ARGS==
parser = argparse.ArgumentParser(
                    prog = 'Server Jn',
                    description = DESCRIPTION,
                    epilog = 'V 1.0')
parser.add_argument('-p', '--port', default=PORT)
parser.add_argument('-i', '--interface', default=INTF)
args = parser.parse_args()
INTF = args.interface
PORT = int(args.port)

# ==CALCULATE FUNCTIONS==
def are_coprime(a,b):
    #TODO: ottimizzare questa funzione
    hcf = 1

    for i in range(1, a+1):
        if a%i==0 and b%i==0:
            hcf = i

    return hcf == 1

def generate_group(n):
    group = list()

    for i in range(1, n):
        if are_coprime(i, n):
            group.append(i)

    return group

def calculate_phi(request):
    n = int(request['n'])
    group = generate_group(n)
    response = {
        'response': len(group)
    }
    return response

def calculate_list_elements(request):
    n = int(request['n'])
    group = generate_group(n)
    response = {
        'response': group
    }
    return response

def calculate_reverse(request):
    pass

# ==COM FUNCTIONS==
def request_handler(request):
    if request['richiesta'] == Req_list.PHI.value:
        response = calculate_phi(request)
    elif request['richiesta'] == Req_list.NUMELEM.value:
        response = calculate_phi(request)
    elif request['richiesta'] == Req_list.CARDINALITY.value:
        response = calculate_phi(request)    
    elif request['richiesta'] == Req_list.ELEMENTS.value:
        response = calculate_list_elements(request)
    elif request['richiesta'] == Req_list.REVERSE.value:
        response = calculate_reverse(request)
    else:
        #TODO: throw error
        response = 'error'
    
    return response

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:    #open connection
        s.bind((INTF, PORT))
        s.listen()
        print(f'[##] Listening on {INTF}:{PORT}')

        while True:
            conn, addr = s.accept()     #accept connection
            print(f"\n[##] Connected by {addr}")
            # data = conn.recv(4096)      #take data

            # Manda l'introduzione
            conn.sendall(DESCRIPTION.encode())
            print(f"[>>] {DESCRIPTION}")


            # Aspetta il JSON del client
            # TODO: aggiungere il timeout 5s
            #s.settimeout(5.0)
            conn.settimeout(5.0)

            data = conn.recv(2048)
            json_str = data.decode()
            print(f"[<<] {json_str}")
            json_msg = json.loads(json_str)

            # DO SOMETHING
            response = request_handler(json_msg)
            response = json.dumps(response)

            conn.sendall(response.encode())
            print(f"[>>] {response}")

            conn.close()

if __name__ == '__main__':
    main()


