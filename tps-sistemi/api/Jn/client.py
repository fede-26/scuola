import socket
import argparse
import json
from enum import StrEnum

# Porta e ip del server di default
SERVER = '127.0.0.1'
PORT = '65020'
REQUEST_TYPE = None
INT_N = 0
INT_X = 0

class Req_list(StrEnum):
    """
    Lista (Enum) delle richieste possibili.
    """
    PHI = 'phi'
    NUMELEM = 'numero di elementi nel gruppo'
    CARDINALITY = 'cardinalità del gruppo'
    ELEMENTS = 'elenco degli elementi'
    REVERSE = 'richiesta dell\'inverso'



def make_request():
    """
    Crea il json da inviare al server.
    """
    request = {
        'n': int(INT_N),
        'richiesta': REQUEST_TYPE.value
    }

    # Se la richiesta è REVERSE, aggiungere il campo 'x'
    if REQUEST_TYPE.name == 'REVERSE':
        if INT_X == None:
            print(f'[!!] ERROR: specificare \'-x\'')
            exit()
        request['x'] = int(INT_X)

    return request

def parse_response(response):
    """
    Parsing della risposta del server.

    Args:
        response (dict): Dizionario contenente la risposta del server.
    """
    if REQUEST_TYPE.name == 'ELEMENTS' and response['response'] == [0]:
        print(f"\n[!!] ERRORE: {response['error']}")
    elif REQUEST_TYPE.name != 'ELEMENTS' and response['response'] == 0:
        print(f"\n[!!] ERRORE: {response['error']}")
    else:
        print(f"\n[--] Risposta: {response['response']}")

def main():
    """
    Funzione main.
    """
    global REQUEST_TYPE
    global SERVER
    global PORT
    global INT_N
    global INT_X
    #Parsing degli argomenti
    # For help type --help
    parser = argparse.ArgumentParser(
                        prog = 'Client Jn',
                        description = 'Client Jn di Federico Zotti',
                        epilog = 'V 1.0')
    parser.add_argument('--ip', default=SERVER, help='IP del server (default: 127.0.0.1)')
    parser.add_argument('--request_type', help='Tipo di richiesta (ometterlo per la lista delle richieste)')
    parser.add_argument('-n', required=True, help='Numero da inviare al server')
    parser.add_argument('-x', help='Numero da inviare al server (in caso di richiesta REVERSE)')
    parser.add_argument('-p', '--port', default=PORT, help='Porta del server (default: 65020)')
    args = parser.parse_args()
    SERVER = args.ip
    PORT = int(args.port)
    INT_N = args.n
    INT_X = args.x

    #se request_type non è specificato elencare tutto le richieste possibili con numeri
    if args.request_type == None:
        print(f'[!!] ERROR: specificare una richiesta')
        print(f'[##] Richieste possibili:')
        for i, req in enumerate(Req_list):
            print(f'[{i}] {req}')
        exit()

    #se request_type è un numero, convertirlo nell'enum richiesta
    if args.request_type.isnumeric() and int(args.request_type) < len(Req_list):
    # loop per trovare la richiesta corrispondente al numero
        for i, req in enumerate(Req_list):
            if i == int(args.request_type):
                REQUEST_TYPE = req
                break

    # Crea il json da inviare al server
    request = make_request()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((SERVER, PORT))
        print(f'[##] Connecting to {SERVER}:{PORT}')

        # Riceve l'introduzione
        data = s.recv(2048)
        data = data.decode()
        print(f"[<<] {data}")

        # Manda il json
        json_msg = request
        json_str = json.dumps(json_msg)
        s.sendall(json_str.encode())
        print(f"[>>] {json_str}")

        # Riceve la risposta
        response_str = s.recv(2048)
        response_str = response_str.decode()
        response = json.loads(response_str)
        print(f"[<<] {response}")

        # Parsing della risposta
        parse_response(response)

if __name__ == "__main__":
    main()
