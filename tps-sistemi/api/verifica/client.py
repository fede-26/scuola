import socket
import json
from random import randint, uniform
import sys
from itertools import cycle
import time

HOST = sys.argv[1]  # The server's hostname or IP address
PORT = int(sys.argv[2])  # The port used by the server
KEY = 0  # Secret key


def encrypt(var, key):
    # Encrypt var using xor encryption
    return bytes(
        a ^ b for a, b in zip(
            var, cycle(str(key).encode())
        )
    )


def msg2json(whatis, data):
    # Takes the command and the data and outputs the json
    msg = {"whatis": whatis, "data": data}
    return json.dumps(msg)


def json2msg(received):
    # Takes the json and outputs the object
    # Exit the program if receives an error
    msg = json.loads(received)
    if msg["status"] != "200":
        print(f"ERR -> {response}")
        exit()
    return msg


def sendToServer(msg, isCrypted=False):
    # input json
    # output object
    print(f">> {msg}")
    msg = msg.encode()
    if isCrypted:
        msg = encrypt(msg, KEY)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.sendall(msg)
        data = s.recv(1024)
        if isCrypted:
            data = encrypt(data, KEY)
        data = data.decode()
        print(f"<< {data}")

        response = json2msg(data)
        return response


def genKey():
    # Generate the key using diffie-hellman
    global KEY
    g = 265
    P = 2**127 - 1
    a = randint(100, 100_000)
    A = (g**a) % P

    # Send the parameters to the server
    msg = msg2json("genkey", [g, P, A])

    response = sendToServer(msg)

    B = int(response["data"])
    KEY = (B**a) % P
    print(f"-- KEY = {KEY}")


def sendRandomValue():
    # Send a random value to test if the encryption works
    x = randint(0, 100)
    msg = msg2json("echo", x)
    response = sendToServer(msg, True)


def sendWorldData():
    # Send temperature, atmospheric pressure and humidity
    # All values are random

    temperature = uniform(15.0, 35.0)
    pressure = uniform(990.0, 1100.0)
    humidity = uniform(60.0, 90.0)

    timestamp = int(time.time())

    payload = {
        "temperature": temperature,
        "pressure": pressure,
        "humidity": humidity,
        "timestamp": timestamp,
    }

    msg = msg2json("worlddata", payload)
    response = sendToServer(msg, True)

def whatToDo():
    # Ask the server what to do (turn on heater, HVAC or open window)
    msg = msg2json("whattodo", "")
    response = sendToServer(msg, True)
    # Do something with response["data"]
    riscaldamento = response["data"]["riscaldamento"]
    condizionatore = response["data"]["condizionatore"]
    finestra = response["data"]["finestra"]

    print(f"-- Riscaldamento -> {riscaldamento}")
    print(f"-- Condizionatore -> {condizionatore}")
    print(f"-- Finestra -> {finestra}")

def main():
    genKey()
    sendRandomValue()
    sendWorldData()
    whatToDo()


if __name__ == "__main__":
    main()
