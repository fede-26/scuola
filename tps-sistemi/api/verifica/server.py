#!/usr/bin/python3

from random import randint
from itertools import cycle
import json
import socket 
import sys

HOST = "0.0.0.0"
PORT = int(sys.argv[1])

KEY = 0

def encrypt(var, key):
    return bytes(a ^ b for a, b in zip(var, cycle(str(key).encode())))

def toJson(status, data):   #from dictionary to json
    msg = {
        "status": status,
        "data": data
    }
    return json.dumps(msg)

def genKey(data):       #generate key using diffie-hellman
    global KEY
    g, P, A = data[0], data[1], data[2]
    b = randint(100, 100000)
    B = ( g**b ) % P
    KEY = ( A**b ) % P
    print("Key = ".format(KEY))
    return B

def meteo(payload):
    with open('dati.txt', 'w') as f:      #write data on file
        for x, y in payload.items():
            f.write(f"{x.capitalize()} + {y:.2f}\n")    #write each item of dict
            print(x, f"{y:.2f}")

def manage():
    options = {     #random parameters
        "riscaldamento": True,
        "condizionatore": False,
        "finestra": False
    }
    return options
            
def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:    #open connection
        s.bind((HOST, PORT))
        s.listen()   

        while True:
            toCrypt = True
            conn, addr = s.accept()     #accept connection
            print(f"Connected by {addr}")
            data = conn.recv(4096)      #take data
            
            if not data:        #if not data send error message
                json_msg = toJson("400", "Message is empty")
                json_msg = json_msg.encode()
            else:
                if not (data[0] == ord("{") and data[-1] == ord("}")):  #if message is encrypted
                    data = encrypt(data, KEY)
                data = data.decode()
                data = json.loads(data)
                
                if data['whatis'] == "genkey":      #if message is genkey don't encrypt the response
                    toCrypt = False
                    json_msg = toJson("200", genKey(data['data']))
                elif data['whatis'] == "echo":
                    json_msg = toJson("200", data["data"])
                elif data['whatis'] == "worlddata":
                    meteo(data['data'])
                    json_msg = toJson("200", "OK - Ghezzi")
                elif data['whatis'] == "whattodo":
                    json_msg = toJson("200", manage())
                else:
                    json_msg = toJson("400", "Command not found")
                    
                print(json_msg)

                json_msg = json_msg.encode()
                if toCrypt:
                    json_msg = encrypt(json_msg, KEY)
                
            conn.sendall(json_msg)
            conn.close()

if __name__ == '__main__':
    main()
