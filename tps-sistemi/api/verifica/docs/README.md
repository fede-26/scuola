---
title: "Protocollo per trasmissione di dati tramite socket TCP e JSON"
author:
  - Federico Zotti (client)
  - Mirko Arrigoni (server)
date: 2022-12-08
titlepage: true
toc: true
lof: true
numbersections: true
documentclass: scrartcl
#documentclass: scrreprt
lang: it
standalone: true
#highlight-style: tango
pdf-engine: xelatex
katex: true
geometry:
  - a4paper
  - margin=4.5cm
pagestyle: headings
colorlinks: true
links-as-notes: true
---

\pagebreak

# Descrizione

Creazione di un protocollo che, tramite socket _TCP_ e utilizzo della codifica _JSON_, permetta di trasmettere dati in maniera sicura e affidabile.
Il **client** è rappresentato da un dispositivo collegato a sensori ambientali e dispositivi di una casa domotica.
Il **server** prende decisioni su quali dispositivi azionare (_tramite il client_) sulla base dei dati ricevuti dai sensori.
Per la socket viene utilizzato IPv4 e si considerano entrambi i dispositivi accessibili tramite IP.

# I comandi

Il client e il server comunicano tra di loro inviandosi un dizionario (o _hashmap_) in JSON.
Dopo ogni comando viene inviata una risposta e la socket viene chiusa.

Il dizionario inviato dal client al server è:
```json
{
  "whatis": comando,
  "data": parametri_del_comando
}
```

`whatis`
:   Rappresenta il comando per il server

`data`
:   Contiene eventuali parametri relativi al comando.

Il dizionario inviato dal server al client è:
```json
{
  "status": codice_errore,
  "data": risposta_del_comando
}
```

`status`
:   Rappresenta il codice di risposta del server.
    Può essere:

    - `200`: non è presente nessun errore;
    - `400`: l'errore è causato dal client;
    - `500`: l'errore è causato dal server;

`data`
:   Contiene eventuali parametri relativi al comando.
    Se `status` è diverso da `200`, contiene l'errore generato.

\pagebreak

## Comando: `genkey`

Il comando `genkey` viene utilizzato per inizializzare una chiave comune utilizzata poi per cifrare tutte le comunicazioni successive.
All'avvio del client viene eseguita, poi la chiave viene salvata e utilizzata fino al termine del programma.

Essendo che anche il server salva la chiave in una variabile, non è attualmente possibile utilizzare due client differenti simultaneamente senza rigenerare la chiave prima di ogni comando, operazione "costosa" in termini di tempo e energia.

Per la generazione della chiave viene usato l'algoritmo [Diffie-Hellman](https://it.wikipedia.org/wiki/Scambio_di_chiavi_Diffie-Hellman) ([implementazione in python](https://pdf.7-bit.xyz/tps/diffie-hellman/diffie-hellman.html)).
Il client e server condividono $g$ e $P$, definiti dal programmatore e costanti.
Il client genera $a$ casuale e calcola $A$, il quale viene inviato al server.
Il server dopodichè genera $b$, calcola $B$, che invia come risposta, e la chiave comune.
Il client riceve $B$ e calcola a sua volta la chiave comune.

### Esempio

Client $\Rightarrow$ Server:
```json
{
  "whatis": "genkey",
  "data": [g, P, A]
}
```

Client $\Leftarrow$ Server:
```json
{
  "status": "200",
  "data": B
}
```

\pagebreak

## Comando: `echo`

`echo` è un comando creato per verificare che la cifratura funzionasse.
L'unica sua funzione è quella di reinviare la stringa inviata con il comando.

### Esempio

Client $\Rightarrow$ Server:
```json
{
  "whatis": "echo",
  "data": "stringa di esempio"
}
```

Client $\Leftarrow$ Server:
```json
{
  "status": "200",
  "data": "stringa di esempio"
}
```

\pagebreak

## Comando: `worlddata`

`worlddata` invia un dizionario di dati ambientali raccolti con i sensori collegati con il client al server.
Attualmente i dati sono generati casualmente in un range verosimile per simulare dei sensori reali.
Viene anche inviato un _timestamp_ in formato [Unix epoch](https://it.wikipedia.org/wiki/Tempo_(Unix)).

Il server salva questi dati in un file di testo per poterli recuperare in un secondo momento.
Al momento però non vengono utilizzati.

### Esempio

Client $\Rightarrow$ Server:
```json
{
  "whatis": "worlddata",
  "data": {
    "temperature": "20.0",
    "humidity": "80.0",
    "pressure": "1000.0",
    "timestamp": "1672663271"
  }
}
```

Client $\Leftarrow$ Server:
```json
{
  "status": "200",
  "data": ""
}
```

\pagebreak

## Comando: `whattodo`

Il comando `whattodo` serve per richiedere quali dispositivi collegati al client azionare.
Attualmente il server invia la stessa lista di azioni, perchè non sono stati implementati algoritmi necessari a calcolare le azioni in base ai dati precedentemente ricevuti.

Il dizionario è composto dai dispositivi e da un valore `bool` nel caso vadano accesi o spenti (o aperti/chiusi se si tratta di finestre).
I dispositivi collegati sono l'impianto di riscaldamento, di condizionamento e le finestre.

### Esempio

Client $\Rightarrow$ Server:
```json
{
  "whatis": "whattodo",
  "data": ""
}
```

Client $\Leftarrow$ Server:
```json
{
  "status": "200",
  "data": {
    "riscaldamento": "true",
    "condizionatore": "false",
    "finestra": "false"
  }
}
```

\pagebreak

# La crittografia XOR

Per cifrare i messaggi abbiamo utilizzato la [crittografia XOR](https://en.wikipedia.org/wiki/XOR_cipher) perchè facilmente implementabile e relativamente semplice rispetto a tecniche più avanzate (DES/AES).
Inoltre può essere usata la stessa funzione sia per criptare che per decifrare il messaggio.

```python
def encrypt(var, key):
    # Encrypt var using xor encryption
    return bytes(
        a ^ b for a, b in zip(
            var, cycle(str(key).encode())
        )
    )
```

Come chiave vengono usati i byte ascii della stringa contenente il numero "chiave".
Questo significa che i possibili byte utilizzati nella chiave effettiva al momento della cifratura non sono tutti 256, ma solo quelli che rappresentano le cifre.
Utilizzando la funzione `cycle` è possibile ripeterla per tutta la lunghezza del messaggio.

Il server controlla se un messaggio è cifrato se inizia con "`{`" e finisce con "`}`".
Se è cifrato tenta di decodificarlo con la chiave memorizzata.

# Eseguire i programmi

Per eseguire i programmi è necessario specificare la porta sulla quale fare la bind della socket (nel server) e i rispettivi IP per creare la socket. 

```bash
python client.py [server-ip] [port]
```

```bash
python server.py [port]
```

# Sequence diagrams

Di seguito elencati i diagrammi per ogni comando.

(generati con [sequencediagram.org](https://sequencediagram.org/)).

![Sequence diagram `genkey`](imgs/genkey.svg){ height=90% }

![Sequence diagram `echo`](imgs/echo.svg){ height=90% }

![Sequence diagram `worlddata`](imgs/worlddata.svg){ height=90% }

![Sequence diagram `whattodo`](imgs/whattodo.svg){ height=90% }