import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class UserTest {

    User user = new User("Federico", "Zotti", "1234567", 10);
    User user2 = new User("Federico", "Zotti", "12345678901", 10);
    User voidUser = new User();


    @Test
    public void testUserVoid(){
        Assertions.assertFalse(voidUser.datiCompleti()); 
    }    
    
    @Test
    public void testApplicaSconto(){
        Assertions.assertEquals(user.applicaSconto(100), 90); 
    }
       
    @Test
    public void testVerificaTelefono(){
        Assertions.assertThrows(RuntimeException.class, new Executable() {
             
            @Override
            public void execute() throws Throwable {
                User user = new User("Federico", "Zotti", "1234567", 10);
                user.verificaTelefono();
                }
        }); 
    }

    @Test
    public void testVerificaTelefono2(){
        Assertions.assertDoesNotThrow(new Executable() {
             
            @Override
            public void execute() throws Throwable {
                User user = new User("Federico", "Zotti", "12345678901", 10);
                user.verificaTelefono();
                }
        }); 
    }


}

