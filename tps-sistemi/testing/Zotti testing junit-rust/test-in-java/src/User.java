
public class User {
    private String nome; 
    private String cognome; 
    private String nTelefono; 
    private float sconto; 

    public User(String nome, String cognome, String nTelefono, float sconto){
        this.nome = nome; 
        this.cognome = cognome;
        this.nTelefono = nTelefono;
        this.sconto = sconto;
    }
    
    public User(){
         this.nome = ""; 
        this.cognome = "";
        this.nTelefono = "";
        this.sconto = 0;
    }

    public Boolean datiCompleti(){
        return !this.nome.equals("") &&
               !this.cognome.equals("") &&
               !this.nTelefono.equals("") &&
               this.sconto >= 0 &&
               this.sconto <= 100;
    }

    public float applicaSconto(float importo){
        return importo - (importo*this.sconto/100);
    }

    public void verificaTelefono() throws RuntimeException{
        if (this.nTelefono.length() < 10){
            throw new RuntimeException("Il numero di telefono deve essere di 10 numeri");
        }
    }

}
