// Struct come alternativa ad una classe,
// può implementare dei metodi pubblici
pub struct User {
    nome: String,
    cognome: String,
    n_telefono: String,
    sconto: f32,
}

impl User {
    pub fn default() -> Self {
        // Crea un User vuoto
        Self {
            nome: String::new(),
            cognome: String::new(),
            n_telefono: String::new(),
            sconto: 0.0,
        }
    }

    pub fn new(nome: String, cognome: String, n_telefono: String, sconto: f32) -> Self {
        // Constructor semplice
        Self {
            nome,
            cognome,
            n_telefono,
            sconto,
        }
    }

    pub fn dati_completi(&self) -> bool {
        // Verifica che i dati siano stati inizializzati
        {
        self.nome != String::new() &&
        self.cognome != String::new() &&
        self.n_telefono != String::new() &&
        0.0 <= self.sconto &&
        self.sconto <= 100.0
        }
    }

    pub fn applica_sconto(&self, importo: f32) -> f32 {
        // Applica lo sconto all'importo
        importo - (importo*self.sconto/100.0)
    }

    pub fn verifica_telefono(&self){
        // Verifica che il numero di telefono sia di almeno 10 numeri
        if self.n_telefono.len() < 10 {
            panic!("Il numero di telefono deve essere di 10 numeri");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    

    #[test]
    fn test_user_void() {
        // Questo test è per verificare se i dati non sono completi
        let user = User::default();
        assert!(!(user.dati_completi()));
    }
    
    #[test]
    fn test_user_not_void() {
        // Questo test è per verificare se i dati sono completi
        let nome = "Federico".to_string();
        let cognome = "Zotti".to_string();
        let n_telefono = "1234567".to_string();
        let sconto:f32 = 10.5;
        let user = User::new(nome, cognome, n_telefono, sconto);
        assert!(user.dati_completi());
    }
    
    #[test]
    fn test_applica_sconto() {
        // Test per applicazione sconto
        let nome = "Federico".to_string();
        let cognome = "Zotti".to_string();
        let n_telefono = "1234567".to_string();
        let sconto:f32 = 10.0;
        let user = User::new(nome, cognome, n_telefono, sconto);
        assert_eq!(user.applica_sconto(100.0), 90.0);
    }

    #[test]
    fn test_verifica_telefono_1() {
        // Test per verifica di un telefono
        let nome = "Federico".to_string();
        let cognome = "Zotti".to_string();
        let n_telefono = "12345678901".to_string();
        let sconto:f32 = 10.0;
        let user = User::new(nome, cognome, n_telefono, sconto);
        user.verifica_telefono();
    }
    #[test]
    #[should_panic]
    fn test_verifica_telefono_2() {
        // Test per verifica di un telefono sbagliato
        let nome = "Federico".to_string();
        let cognome = "Zotti".to_string();
        let n_telefono = "1234567".to_string();
        let sconto:f32 = 10.0;
        let user = User::new(nome, cognome, n_telefono, sconto);
        user.verifica_telefono();
    }
}