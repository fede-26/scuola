pub mod user;
use user::User;

fn main() {
    //funzione main, non fa niente
    println!("Hello, world!");
    let nome = "Federico".to_string();
    let cognome = "Zotti".to_string();
    let n_telefono = "1234567".to_string();
    let sconto:f32 = 10.5;
    let _user = User::new(nome, cognome, n_telefono, sconto);
}
